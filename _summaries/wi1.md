---
title: Wirtschaftsinformatik 1
layout: post
id: wi1
date: "2016-10-11"
---
* toc
{:toc}

## Wirtschaft und Organisation

### Wirtschaften
![](/assets/images/summaries/{{page.id}}/image001.png)

![](/assets/images/summaries/{{page.id}}/image003.png)

### Ökonomisches Prinzip

![](/assets/images/summaries/{{page.id}}/image005.png)

### Ziele, Zielbeziehungen der Unternehmung

![](/assets/images/summaries/{{page.id}}/image007.png)

Beispiel für Indifferent:

Liquidität erhöhen (Finanzwirtschaftlich) und Produkqualität erhöhen.

![](/assets/images/summaries/{{page.id}}/image009.png)

### Offener Wirschaftskreislauf
![](/assets/images/summaries/{{page.id}}/image011.png)

### St. Galler Management Model
![](/assets/images/summaries/{{page.id}}/image013.png)

Natur hat einen sehr schlechten Stellenwert (ist ja gratis). Kunden sind vermutlich die wichtigstens Partner, da sie unsere Existenzberechtigung sind.

### Organisation
![](/assets/images/summaries/{{page.id}}/image015.png)

Informale Organisation bilden den grössten Teil der Organisation (Kaffepausen, Treffen ausserhalb von Arbeitszeiten, ...) Können aber auch negative Auswirkungen haben.

### Verbindungswege
![](/assets/images/summaries/{{page.id}}/image017.png)

### Aufbauorganisation
Strukturierung des Unternehmens in organisatorische Einheiten (Stellen, Abteilungen).

![](/assets/images/summaries/{{page.id}}/image019.png)

Je weiter oben die Aufgaben liegen, desto abstrakter sind diese (desto weniger Informationen werden einem gegeben)

### Business Process Reengineering

- Fundamentale Änderungen am Unternehmen oder Unternehmensprozessen
- **Resultat:** Aussergewöhnliche Verbesserungen im Bereich Kosten, Qualität, Service und Zeit
- **Schwergewicht** liegt auf **Identifikation** von Kernprozessen im Rahmen der Wertschöpfungskette
- **Kernprozesse** bestehen aus einem Bündel funktionsübergreifender Tätigkeiten und sind darauf ausgeriechtet einen Kundenwert zu schaffen

**Beispiele** Kunde neu abholen. **Uber**, **Tinder** (Kombination aus bestehendem und Mobiltelefon)

![](/assets/images/summaries/{{page.id}}/image021.png)

Oft werden externe Leute dafür verwendet (Spinner) welche frischen Wind in ein Unternehmen bringen.

### Aufbau &lt;&gt; ABlauforganisation

![](/assets/images/summaries/{{page.id}}/image023.png)

**Übersicht Organisationsinstrumente**

- Aufbauorganisation 
   - Organigramm
   - Stellenbeschreibung
   - Funktionendiagramm
- Ablaufplan
   - Balkendiagramm
   - Netzplan

Ein **Funktionendagramm** zeigt in einer Matrix welche Stellen für welche Aufgaben welche Verantwortung
haben, (Planen, Entscheidung, Mitspracherecht, Ausführen)

Ein **Ablaufplan** zeigt welche Stellen in welcher Reihenfolge bei der Erfüllung einer bestimmten Aufgabe beteiligt sind.

### Organisationsgrad
![](/assets/images/summaries/{{page.id}}/image025.png)

## Markt

### Marktformen
![](/assets/images/summaries/{{page.id}}/image027.png)

**Vollständige Konkurrenz**

Pizzabäcker, Tankstellen, Putzfirmen, Coiffure

**Angebots Oligopol**

Kaffee mit Sirup, Busse

**Angebotsmonopol**

ÖV (ZÜge), SBB, Cablecom

**Nachfrageoligopol**

Elektro Fahrräder, Molkerei (Emmi)

**Bilaterales Oligopol**

Luxus Uhren, Chemie-Grundstoffe

**Beschränktes Angebotsmonopol**

Seltene Erden

**Nachfragemonopol (Monopson)**

Militär

**Beschränktes Nachfragemonopol**

Glasfaserkabel, Grosse Stromleitung

**Bilaterales Monopol**

Kernkraftwerk, Angereichertes Uran, Notendruck (Orell Füssli)

### Preis-Absatzfunktion &amp; Umsatz
![](/assets/images/summaries/{{page.id}}/image029.png)

![](/assets/images/summaries/{{page.id}}/image031.png)

### Preiselastizität

![](/assets/images/summaries/{{page.id}}/image033.png)

Sobald die &quot;Magische&quot; Grenze überschritten wird, wird die Fläche (rechteck) kleiner. Es gilt, einen optimalen Preis zu bestimmen, um einen grösstmöglichen Absatz zu erreichen.

![](/assets/images/summaries/{{page.id}}/image035.png)

### Anomale Preiselastizitäten

![](/assets/images/summaries/{{page.id}}/image037.png)

**Beispiele**
**e &lt; -1**

Es gibt viele Substitute, z.B. Brot. Bei Preiserh�hung gehen alle zur Konkurrenz

**-1 &lt; e &lt; 0**

Lebensnotwenduge Güter, z.B. Benzin

**e = -∞**

Gold wird unter oder über dem Marktpreis angeboten. Zehnernote für neun Franken.

**e = 0**

Sucht, Insulin, Sammlerstücke

**e &gt; 0**

Statusobjekte, Snobeffekt

### Unternehmensangebot zum Marktangebot

![](/assets/images/summaries/{{page.id}}/image039.png)

Normalerweise nimmt man mehr als nur zwei Anbieter.

### Angebotsverhalten bei linearen Kostenfunktionen

![](/assets/images/summaries/{{page.id}}/image041.png)

### Spinnweb Theorem
![](/assets/images/summaries/{{page.id}}/image043.png)

Kurve 1 unerwünscht, Anpassungen immer stärker

Kurve 2 erwünscht, Preis pendelt sich ein.

Break Even: Erlös = Selbstkosten

![](/assets/images/summaries/{{page.id}}/image045.png)

### Optimale Losgrösse

![](/assets/images/summaries/{{page.id}}/image047.png)

Da ein Lager immer Voll und dann wieder leer wird sind die Kosten die der
durchschnittlichen gelagerten Einheiten (/2).

## Operative Anwendungssysteme

![](/assets/images/summaries/{{page.id}}/image049.png)

**Rationalisierungseffekt** Zeitgewinn für die abarbeitung einer Aufgabe. Je besser ein Anwendungssystem, desto weniger Zeit wird gebraucht.

**Groupware** kann eingesetzt werden für: Workflowmanagement, Dokumentmanagement, Wissensmanagement, Konferezen, Arbeitsräume.

### Maximal umsatz
Bekannt: Preis-Absatz (a und b)

> U = p * x = (20 - 1/5 * x) * x = 20x - 1/5x2
> 
> U' = 20 - 2/5x = 0 (für max)

> x = 50
> 
> p = 20 - 1/5 * x (jetzt mit x = 50)

> p = 20 - 1/5 * 50 = 10

## Fertigungsindustrie
![](/assets/images/summaries/{{page.id}}/image051.png)

## Portfolio analyse
![](/assets/images/summaries/{{page.id}}/image053.png)

short selling ist ein risikoreiches aber auch sehr gewinnbringendes Geschäft. Man verkauft Aktien welche einem noch nicht gehören und hofft das man diese beim Zeitpunkt des Verkauft günstiger bekommt.

## Analytische
Anwendungssysteme

![](/assets/images/summaries/{{page.id}}/image055.png)

![](/assets/images/summaries/{{page.id}}/image057.png)

![](/assets/images/summaries/{{page.id}}/image059.png)

![](/assets/images/summaries/{{page.id}}/image061.png)

### Harmonisierungen

**Unterschiedliche Codierungen**
Gleiches Attibut, Gleiche Bedeutung, verschiedene Domäne. (m/w und 0/1) → wahl einer Domäne

**Synonyme** 
Verschiedene Attributnamen, gleicher Inhalt → wahl eines Attibutnamens

**Homonyme**
Gleiche Attibutnamen mit unterschidlichen Inhalten → teilen in zwei eigene Attibute

**Anreicherungen**
Zahlen zusammenzählen und Werte berechnen, aus Umsatz und Kosten kann der Gewinn berechnet werden.

**Metadaten**
Ermöglichen das wiederauffinden von Informationen für den Endbenutzer.

### Vergleich der 3 Konzepte

![](/assets/images/summaries/{{page.id}}/image063.png)

**OLAP:**
Online Analytical Processing

**Anforderungen an OLAP**
1. Mehrdimensionale konzeptionelle Sicht auf die Daten 
2. Transparenz 
3. Zugriffsm�glichkeiten
4. Stabile Antwortzeiten 
5. Client-Server Architektur
6. Gleichrangige Dimensionen
7. Dynamische Handhabung "dünn besetzter" Matrizen
8. Mehrbenutzerf�higkeit
9. Unbeschr�nkte dimensions�bergreifende Operationen
10. Intuitive Datenanalyse
11. Flexibles Berichtswesen
12. Unbegrenzte Anzahl von Dimensionen und Aggregationsstufen 

**FASMI Anforderungen an OLAP**

**F** ast

**A** nalysis

**S** hared

**M** ultidimensional

**I** nformation

### Navigation in OLAP

![](/assets/images/summaries/{{page.id}}/image065.png)

### STAR-Schema

Zentrale Faktentabelle und diese Fakten werden Sinnvoll in Dimensionstabellen gruppiert
(Zeit, Ort, Produkt,  Vertragstyp, ...)

### Snowflake-Schema

Wie das **Star-Schema** nur können hier Dimensionstabellen wieder als Faktentabelle verwendet werden. (Region -> Land, Bezirk, Stadt)

### Datenanalyse-zyklus

![](/assets/images/summaries/{{page.id}}/image067.png)

### Assoziazionsanalyse

![](/assets/images/summaries/{{page.id}}/image069.png)

### Managementaufgaben

![](/assets/images/summaries/{{page.id}}/image071.png)

Beim Überwachen werden einfache Meldungen verwendet, zB Ampelsystem für Soll-Ist vergleich.

### Arten von Kennzahlen

![](/assets/images/summaries/{{page.id}}/image073.png)

### Integration

Integration bezeichnet in der Wirtchaftsinformatik die Verknüpfung von Menschen, Aufgaben und Technik zu einem einheitlichen Ganzen, um den Folgen der durch Arbeitsteilung und Spezialisierung entstandenen Funktions-, Prozess und Abteilungsgrenzen entgegenzuwirken.

### Integreationsdimensionen

![](/assets/images/summaries/{{page.id}}/image075.png)

Der Auftragsbearbeitungsprozess 

9.6 

Name 

(Definition)
Unternehmensweite
Anwendungssysteme 

**Systeme** mit denen Aktivitäten, Entscheidungen und Kenntnisse über viele verschiedene Funktionen, Ebenen und Geschäftseinheiten hinweg in einem Unternehmen koordiniert werden können. Hierzu gehören ERP-Systeme, Supply-Chain-Management-Systeme, Systeme zum Management von Kundenbeziehungen, Wissensmanagementsysteme und Systeme für die (Gruppen-)Zusammenarbeit.

**Daten**
Gemeinsame Nutzung derselben Daten durch verschiedene Funktionen. Redundante Speicherung vermindern die zu Dateninkonsistenz führen würde.

**Inkonsistenz**
tritt dann auf, wenn die selben Daten an mehreren Orten vorhanden sind, beim Anpassen aber nicht an allen Orten die Änderungen vorgenommen werden.

**Fehlerhafte Integrität**
ist dann der Fall, wenn Daten aus dem einen System auf Daten eines anderen verweisen, welche nicht oder nicht mehr verfügbar sind.

**Lösung:**
Datenbanken für Datenhaltung einsetzen.

### Integrationsorientierung

![](/assets/images/summaries/{{page.id}}/image077.png)

### Optimaler INtegrationsgrad

![](/assets/images/summaries/{{page.id}}/image079.png)

### Unternehmensweite
Anwendungssysteme

Systeme, mit denen Aktivitäten, Entscheidungen und Kenntnisse über viele verschiedene Funktionen, Ebenen und Geschäftseinheiten hinweg in einem Unternehmen koordiniert werden können. Hierzu gehören ERP-Systeme, Supply-Chain-Management-Systeme, Systeme zum Management von Kundenbeziehungen,Wissensmanagementsysteme und Systeme für die (Gruppen-)Zusammenarbeit.

### Überbetrieblicher Material- und Informationsfluss

![](/assets/images/summaries/{{page.id}}/image081.png)

### ERP Systeme

Enterprise Resource Planning Systeme

Integrierte unternehmensweite Anwendungssysteme, die zur Koordination wichtiger interner
Prozesse eines Unternehmens dienen.

![](/assets/images/summaries/{{page.id}}/image083.png)

---
title: Informationssicherheit 3
layout: post
id: infsi3
date: "2017-08-11"
---
## Information Security Management
> Risk = Value * Threat * Vulnerability

![](/assets/summaries/{{page.id}}/image001.png)

**Assets / Values**
Assets can be of a material nature as money and goods or valuable data as e.g. intellectual property, business records, customer or inventory data but also an organization's public reputation or credibility is a valuable asset.

**Vulnerability**
A vulnerability is a IT security weakness that could be exploited to endanger or cause harm to an asset.

**Threat**
A threat is an actual exploit of a detected vulnerability

**Risk**
Risk is the likelyhood that something bad will happen that causes harm to an asset or the loss thereof, multiplied by the amount of possible dammage.

Risk can be minimized by either
- minimizing the threat or the amount (value) of encurred loss. This can not be influenced mostly.
- reducing the likelyhood of a successful attack by taking security protection measures, e.g. by reducing the number and severity of vulnerabilities.

The cost of security measures increase drastically with increasing levels of security but cause a monotonously decreasing cost of suffered incidents.

The optimum security level minimizing the overall cost can always be found although the actual computation of the **Return on Security Investment** (ROSI) is quite a tricky task due to the often unknown probabilities.

### Damage Indicators

| Indikator | Masseinheit | Bagatelle | Unfall | Störfall | Katasrophe |
| --------- | ----------- | --------- | ------ | -------- | ---------- |
| Sach- und Vermögenswerte | % Umsatz | < Tagesumsatz | Monatsumsatz | Quartalsumsatz | > Jahresumsatz |
| Image | öffentliche Wahrnehmung, politischer Druck | schlechte Presse | Kundenverluste, politischer Pressionen | Auswechslung des Managements | Schliessung des Betriebs |
| Legalität | juristische Reaktionen | informelle Reklamationen | Klagen | Verurteilungen (Gefängnis/Busse) | Verurteilungen (Zuchthaus) |
| Leib und Leben | Tote und Verletzte | 1-3 Verletze | 3-10 Verletze 1 Toter | 2-3 Tote | 4-10 Tote |
| Umwelt |  Beeinträchtigung von Boden und Gewässern | < 1 ha reversibel | 1-10 ha reversibel | > 10 ha reversibel < 1 ha irreversibel | > 1 ha irreversibel |

Some Companies underly certain compliance laws and regulations
- information security companies
- managment of personal and sensitive data
  - law books
  - privacy laws
  - health insurance portability and accountability act
- finance companies
  - bank laws
  - EU directive on payment services
  - payment card industry data security standard
- telecommunication
  - Fernmeldegesetz
  - lawful interception
- general controlling
  - Sarbanes-Oxley Act
  - US stock quoted companies

### Datenschutz
Art. 8 - Allgemeine Massnahmen

> Wer als Privatperson Personendaten bearbeitet oder ein Datenkommunikationsnetz zur Verfügung stellt, sorgt für die Vertraulichkeit, die Verfügbarkeit und die Richtigkeit der Daten, um einen angemessenen Datenschutz zu gewährleisten. Insbesondere schützt er die Systeme gegen folgende Risiken:

> 1. unbefugte oder zufällige Vernichtung
> 2. zufälliger Verlust
> 3. technische Fehler
> 4. Fälschung, Diebstahl oder widerrechtliche Verwendung
> 5. unbefugtes Ändern, KOpieren, Zugreifen oder andere unbefugte Bearbeitungen

> Die technischen und organisatorischen Massnahmen müssen angemessen sein. Insbesondere tragen sie folgenden Kriterien Rechnung:

> 1. Zweck der Datenbearbeitung
> 2. Art und Umfang der Datenbearbeitung
> 3. Einschätzung der möglichen Risiken für die betroffenen Personen
> 4. gegenwärtiger Stand der Technik

> Diese Massnahmen sind periodisch zu überprüfen

> Der Beauftragte kann in diesem Bereich Empfehlungen in Form von Handbüchern erlassen

## Software Security
Building Security In

### Top 25 Dangerous Software Errors
In ways in which data is sent and received between separate components, modules, programs, processes, threads or systems.

- (1) Improper neutralization of special elements in an SQL command (SQL Injection)
- (2) Improper neutralization of special elements in an OS command (OS Command Injection)
- (4) Improper neutralization of input during web page generation (Cross-site scripting)
- (9) Unrestricted upload of file with dangerous type
- (12) Cross-site request Forgery (CSRF)
- (22) URL redirection to untrusted site (Open redirect)

In ways in which software does not properly manage the creation, usage, transfer or destruction of important system resources.

- (3) Buffer Copy without checking size of input (Buffer overflow)
- (13) Improper limitation of a pathname to a restricted directory
- (14) Download of code without integrity check
- (16) Inclusion of functionality from untrusted control sphere
- (18) Use of potentially dangerous functions
- (20) Incorrect calculation of buffer size
- (23) Uncontrolled format string
- (24) Integer overflow or wraparound

Defensive strategies or techniques that are often misused, abused or just plain ignored.

- (5) Missing authentication for critical functions
- (6) Missing authorization
- (7) Use of hard-coded credentials
- (8) Missing encryption of sensitive data
- (10) Reliance on untrusted inputs in a security decision
- (11) Execution with unnecessary privileges
- (15) Incorrect authorization
- (17) Inorrect premission assignment for critical ressource
- (19) Use of a broken or risky cryptographic algorithm
- (21) Improper restriction of excessive authentication attempts
- (25) Use  of a one-way hash without a salt

### Trinity of trouble
- **Connectivity**
  - Gives a larger number of attack vectors and adds ease on how an attack can be made. Due to SOA, legacy applications that were never intended to be internetworked are now published as services.
- **Extensibility**
  - The plug-in architecture of web browsers allows easy installation of viewer extensions for new document types.
  - OS support extensibility through dynamically laodable device drivers and modules
  - Applications support extensibility through scripting, controls, components and applets thanks to Java and the .NET framework
- **Complexity**
  - Unrestrained growth in the size and complexity of modern software systems. In practice the defect rate tends to go up as the square of code size

### Bugs, Flaws, Defects
> Bugs + Flaws = Defects

- **Security Bug**
  - A security bug is an implementation-level vulnerability that can be easily discovered and remedied using modern code review tools.
  - Examples: buffer overflows, race conditions, unsafe system calls.
- **Security Flaw**
  - A security flaw is a design-level vulnerability that cannot be detected by automated tools (yet) but usually requires a manual risk-analysis of the software architecture done by experts.
  - Examples: method overriding, error handling, type safety confusion
- **Security Defect**
  - Both bugs and flaws are defects which may lie dormant in software for years only to surface in fielded systems with major consequences
  - In practice software security problems are divided 50/50 between bugs and flaws.



### Cost of Change law

## Microsoft Security Development Lifecycle

## Web Security

### Same Origin Policy

### CORS

### CSP

### XSRF

## Mobile Security

## Reverse Proxy Web App Firewall

## XSS Street Fight

## HTTP Response Splitting

## Mass Assignment Vulnerability

## Server Security

### SSL-TLS

## Fraud Detection

### Markov Chain

### Markov Shield

### APT Detection
Advanced Persistent Threat

## Security Testing

## Identity and Authentication Management

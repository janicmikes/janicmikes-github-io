##Angular



![](/assets/summaries/{{page.id}}/image053.png) 
Angular is made for CRUD applications. It is based on Typescript 2, reduces boilerplate code and comes with DI mechanism. It provides fast 2way binding, is clearly structured and supports information hiding principle on multiple levels of abstraction. It increases the testability and mainainability of the client-side code and provides a framework that covers a wide range of topics from templating to data-services. (although it does not exactly
specified how to wire them) A SPA framework allowing you to use the latest WebEng. principles. 

Metadata describes a class and tells Angular how to process it

![](/assets/summaries/{{page.id}}/image055.png) 
![](/assets/summaries/{{page.id}}/image056.png)
![](/assets/summaries/{{page.id}}/image057.png)

### Command Line Interface
```
//new Module
ng g module core
// new Service
ng g service
```
### Lifecycle




@Component({...})
  export class CounterComponent
  implements OnInit, OnDestroy { 
    ngOnInit(){
      console.log("OnInit");
    }
    ngOnDestroy(){
      console.log("OnDestroy");
    }
  }

### Modules



**Modules** cohesive block of code dedicated to closely related set of
capabilities.
**Barrel-Export** is used by library modules to have a single export with all
containing features (classes, funcs). 
**Root-Module** is present in every Angular App.
**Angular Module vs. ES6-Modules** in Angular, each file represents exactly one module and a module is a logical block of multiple ES6 modules linked together. 
**Library Modules** may accomodate multiple Angular modules -> see barrel-export. Angular itself ships with multiple library modules. They begin with @angular/[core, common, http, router]

![](/assets/summaries/{{page.id}}/image058.png) 
![](/assets/summaries/{{page.id}}/image059.png)

**default import** import all components, pipes and directives
from a given ForeignModule 

**forChild** a static method on a module class (nearly the same as default) allows
configuration of service for the current module level 

**forRoot** static method on a module. Provides and configures services
at the same time. Exclude a provider from the module declaration and add it
directly on root level instead. Enforces that same provider won�t be loaded
twice by lazy modules.  
<span style="color: purple;">
Only root-modules (App Module) should import foreign modules by calling forRoot(). Declare providers in @NgModule declaration OR in forRoot definition but NEVER in both.
</span>

#### Module Types
**Root Module** Provides the entry point (bootstrap component) for the app. Does not need to export anything. Named AppModule within app.module.ts. Specify singleton services here 

**Feature Modules** specifies clear boundaries between the application features. Each dev team can handle ist own feature module. Extend the App. Expose / Hide ist implementation from other modules. (forms, http, routing)

**Routing Modules** specifies the routing specific configuration settings of the Feature (or Root) module

**Shared Module** holds the common components/directives/pipes and shares them with the modules that need them. Do not specify app-wide singleton providers in a SM. LM will create ist own copy of the service when loaded. 

**Core Module** keeps your root Module clean. Contains components/directives/pipes used by the root module additionally, globally used services can be declared here. Only the
rootModule should import the Core Module. Guard against a duplicated import and
fail fast by adding a coremodule constructor

**Lazy Modules** represents lazily loaded feature modules. (Like FeatureModule). Reduce initial footprint. Loaded when invoked by a lazy route. (Coupled with the router)

![](/assets/summaries/{{page.id}}/image061.png)


<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Components</h2>



<!--[if gte vml 1]><v:shape id="Picture_x0020_31" o:spid="_x0000_s1040"
 type="#_x0000_t75" style='position:absolute;left:0;text-align:left;
 margin-left:149.2pt;margin-top:.3pt;width:42pt;height:23.15pt;z-index:251660288;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:page;
 mso-height-relative:page'>
 <v:imagedata ![](/assets/summaries/{{page.id}}/image062.png" o:title=""/>
 <w:wrap type="tight"/>
</v:shape><![endif]--><img width=42 height=23
![](/assets/summaries/{{page.id}}/image063.png) v:shapes="Picture_x0020_31"><![endif]>
Components component is a
directive-with-a-template it controls a section of the view. It is the <b
style='mso-bidi-font-weight:normal'>C known in MV<b style='mso-bidi-font-weight:
normal'>C. Ist assosiation is exactly one view. (Template URL or inline
template = multi-line template). Selector
is used to embedd the component on another view (template) 
 
mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
 tag name or css selector  class=Code> 
style='border:none'> style='border:none'>&lt;mytag&gt; / #mytag
 <b
style='mso-bidi-font-weight:normal'> style='mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'>Usage style='mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'> to use a Component simply target the
componenets selector.  class=Code> 
style='border:none'> style='border:none'>&lt;my-tagname&gt;&lt;/my-tagname&gt;
class=Code>  style='border:none'><o:p></o:p>

ComponentTransclusion
 content within a components selector is passed to the comp. 
class=Code>  style='border:none'> style='border:none'>&lt;my&gt;&lt;h2
class=Code>  style='border:none'> style='border:none'>
title class=Code>  style='border:none'>
style='border:none'>&gt;heading&lt;/h2&gt;&lt;menu&gt;stuff&lt;/menu&gt;&lt;/my&gt;
 using  class=Code>  style='border:none'>
style='border:none'>&lt;ng-content select='[
class=Code>  style='border:none'> style='border:none'>title
class=Code>  style='border:none'> style='border:none'>]'
class=Code>  style='border:none'> style='border:none'>&gt;
 and  class=Code>  style='border:none'>
style='border:none'>&lt;ng-content select='menu'&gt;
 ommit select for all content.

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst><!--[if gte vml 1]><v:shape id="Picture_x0020_32"
 o:spid="_x0000_s1039" type="#_x0000_t75" style='position:absolute;
 margin-left:132.8pt;margin-top:2.95pt;width:50.5pt;height:48.85pt;z-index:251661312;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:page;
 mso-height-relative:page' stroked="t" strokecolor="#deeaf6 [660]"
 strokeweight="1.5pt">
 <v:imagedata ![](/assets/summaries/{{page.id}}/image064.png" o:title=""/>
</v:shape><![endif]--> style='mso-ignore:vglayout;position:
relative;z-index:251661312'> style='position:absolute;left:120px;
top:-844px;width:56px;height:55px'><img width=56 height=55
![](/assets/summaries/{{page.id}}/image065.png" v:shapes="Picture_x0020_32"><![endif]>
@Component({


style='mso-spacerun:yes'>&nbsp; selector: 'wed-navigation', 


style='mso-spacerun:yes'>&nbsp; templateUrl: './nav.component.html',


style='mso-spacerun:yes'>&nbsp; styleUrl: ['./nav.component.css']

})

export class NavigationComponent
{

<p class=CodeBlockCxSpLast>}



<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### Registration</h3>



Components must be declared within the
containing module so ist selector is registered for all sub-components of that
module. They can be exported, so other modules can import and re-use them.

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'><!--[if gte vml 1]><v:shape id="Picture_x0020_33" o:spid="_x0000_i1028"
 type="#_x0000_t75" style='width:185pt;height:85pt;visibility:visible;
 mso-wrap-style:square'>
 <v:imagedata ![](/assets/summaries/{{page.id}}/image066.png" o:title=""/>
</v:shape><![endif]--><img width=185 height=85
![](/assets/summaries/{{page.id}}/image067.png" v:shapes="Picture_x0020_33"><![endif]>

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Templates</h2>



Templates
 a form of HTML telling Angular how to render a component it is the <b
style='mso-bidi-font-weight:normal'>V in the MVC Pattern / HTML annotated
with Angular template syntax / tells Angular how to render the component / can
be embedded in other components   style='font-family:
Wingdings;"Gill Sans";
mso-char-type:symbol; style='mso-char-type:
symbol;�
component tree / HTML tags are allowed but &lt;script&gt;
is <u>forbidden</u> / head, body is nonsense. <b style='mso-bidi-font-weight:
normal'>Interpolation  class=Code> 
style='border:none'> style='border:none'>{{...}}
 Tpl Expressions, binding syntax, <b style='mso-bidi-font-weight:
normal'>Directives provide instructions to transform the DOM, tpl reference
variables.

<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### Template Prinzipien</h3>



Binding
 Mechanism for cordinating what users see with applicaiton data
values. Makes it easier to write, read and maintain when using binding
framework. One-way data
 
mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
view: property / One-way
view  mso-ascii-font-family:
"Gill Sans";mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
data: event binding / Two-way:
property and update event. Private and public prop. of the declaring comp. Can
be bound and other comp. Within view. 

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>// Two-way style='mso-tab-count:
2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b
style='mso-bidi-font-weight:normal'>use [( )]

&lt;input
type=&quot;text&quot; <b>[(ngModel<b>)]=&quot; style='mso-bidi-font-style:
italic'>counter.team&quot;&gt; <o:p></o:p>

// One-way (View
 mso-ascii-font-family:Consolas;
Consolas;mso-char-type:symbol;mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
Model)  style='mso-tab-count:1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b
style='mso-bidi-font-weight:normal'>use ( )

&lt;button
(click)=&quot;counter.eventHandler($event)&quot;&gt;

// One-way (Model
 mso-ascii-font-family:Consolas;
Consolas;mso-char-type:symbol;mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
View)  style='mso-tab-count:1'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b
style='mso-bidi-font-weight:normal'>use [ ] or {{ }}

&lt;p&gt;... {{counter.team}}
..&lt;/p&gt;

<p class=CodeBlockCxSpLast>&lt;img
[attr.alt]=&quot;counter.team&quot; src=&quot;team.jpg&quot;&gt;



Interpolation
 represents a One-way from data to view binding. (the most used
case) can be combined with expressions like ( class=Code>
 style='border:none'> style='border:none'>++/--/new/=/+=/+=/,/;/...
) ( class=Code>  style='border:none'>
style='border:none'>|/&amp;/?) have other
meaning. To bind to a components directive the parameters need to be declared
with @Output for events or @Input for props.  class=Code>
 style='border:none'> style='border:none'>&lt;my
(click)=&quot;&quot; [title]=&quot;&quot;&gt;&lt;/my&gt;
class=Code>  style='border:none'><o:p></o:p>

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>@Component({...})

export class NavigationComponent
{ 


style='mso-spacerun:yes'>&nbsp; @Output() click = new EventEmitter&lt;any&gt;();



style='mso-spacerun:yes'>&nbsp; @Input() title: string;

<p class=CodeBlockCxSpLast>}



Directives
 similar to component but without a template / declared with
@Directive(). <br>
- Structural-directives: modifies
the structure of your DOM (*ngFor/*ngIf)

- Attribute-directives:
alter the appearance or behaviour of an existing element.

Tmeplate-Reference-Variables
 stuff can be referenced by using the # notation 
class=Code>  style='border:none'> style='border:none'>&lt;input
... #id&gt; and then 
class=Code>  style='border:none'> style='border:none'>&lt;button
(click)= class=Code> 
style='border:none'> style='border:none'>cal
class=Code>  style='border:none'> style='border:none'>(
class=Code>  style='border:none'> style='border:none'>id
class=Code>  style='border:none'> style='border:none'>.value)&quot;&gt;Call&lt;/button&gt;

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>@Component(...)

EN-GB'>_ style='mso-fareast-font-family:"MS Mincho";mso-bidi-font-family:
"MS Mincho";mso-ansi-language:EN-GB'><o:p></o:p>

<b> style='mso-bidi-font-family:Times;
color:#00006D;mso-ansi-language:EN-GB'>export class 
style='mso-ansi-language:EN-GB'>CounterComponent <b>
style='mso-bidi-font-family:Times;color:#00006D;mso-ansi-language:EN-GB'>implements
OnInit { 
style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>

<!--[if gte vml 1]><v:shape id="Picture_x0020_36"
 o:spid="_x0000_s1038" type="#_x0000_t75" style='position:absolute;
 margin-left:94.6pt;margin-top:5.3pt;width:100.5pt;height:50.35pt;z-index:251662336;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:page;
 mso-height-relative:page' wrapcoords="2203 944 983 20718 19136 20718 19136 820 2203 944">
 <v:imagedata ![](/assets/summaries/{{page.id}}/image068.png" o:title=""/>
 <w:wrap type="tight"/>
</v:shape><![endif]--><img width=100 height=51
![](/assets/summaries/{{page.id}}/image069.png) v:shapes="Picture_x0020_36"><![endif]><b>
style='mso-bidi-font-family:Times;color:#00006D;mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp; private <b>
style='mso-bidi-font-family:Times;color:#520067;mso-ansi-language:EN-GB'>counter
style='mso-ansi-language:EN-GB'>:CounterModel = <b>
style='mso-bidi-font-family:Times;color:#00006D;mso-ansi-language:EN-GB'>new 
style='mso-ansi-language:EN-GB'>CounterModel();  style='font-size:
12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>

<b> style='mso-bidi-font-family:Times;
color:#00006D;mso-ansi-language:EN-GB'> style='mso-spacerun:yes'>&nbsp;
private  style='color:#676834;mso-ansi-language:EN-GB'>up
style='mso-ansi-language:EN-GB'>(event:UIEvent): <b>
style='mso-bidi-font-family:Times;color:#00006D;mso-ansi-language:EN-GB'>void
style='mso-ansi-language:EN-GB'>{ <o:p></o:p>


style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; <b>
style='mso-bidi-font-family:Times;color:#00006D;mso-ansi-language:EN-GB'>this
style='mso-ansi-language:EN-GB'>.<b> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>counter
style='mso-ansi-language:EN-GB'>.<b> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>count
style='mso-ansi-language:EN-GB'>++;<o:p></o:p>


style='mso-spacerun:yes'>&nbsp;&nbsp; 
style='mso-spacerun:yes'>&nbsp;event. style='color:#676834'>preventDefault();<o:p></o:p>


style='mso-spacerun:yes'>&nbsp; }<o:p></o:p>

<p class=CodeBlockCxSpLast>}
style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Service</h2>



Services
 provide logic of any value, function or feature that your
application needs. / Types can be: logging, data, message bus, tax calc, app
config / coupled to DI / new Components are provided with services by DI,
therefor services must be registered in DI container (add in the providers
array.  class=Code>  style='border:none'>
style='border:none'>@Injectable()
decorator is used. 

<!--[if gte vml 1]><v:shape id="Picture_x0020_39" o:spid="_x0000_s1037"
 type="#_x0000_t75" style='position:absolute;left:0;text-align:left;
 margin-left:85.95pt;margin-top:6.8pt;width:108.05pt;height:45.2pt;z-index:-251652096;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:page;
 mso-height-relative:page'>
 <v:imagedata ![](/assets/summaries/{{page.id}}/image070.png" o:title=""/>
</v:shape><![endif]--> style='mso-ignore:vglayout;position:
relative;z-index:-1895819264'> style='left:0px;position:absolute;
left:76px;top:-956px;width:108px;height:45px'><img width=108 height=45
![](/assets/summaries/{{page.id}}/image071.png" v:shapes="Picture_x0020_39"><![endif]><!--[if gte vml 1]><o:wrapblock><v:shape
  id="Picture_x0020_38" o:spid="_x0000_s1036" type="#_x0000_t75" style='position:absolute;
  left:0;text-align:left;margin-left:-4.45pt;margin-top:11.8pt;width:93.25pt;
  height:37.5pt;z-index:251665408;visibility:visible;mso-wrap-style:square;
  mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
  mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
  mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
  mso-position-horizontal-relative:text;mso-position-vertical:absolute;
  mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent:0;
  mso-width-relative:page;mso-height-relative:page'>
  <v:imagedata ![](/assets/summaries/{{page.id}}/image072.png" o:title=""/>
  <w:wrap type="topAndBottom"/>
 </v:shape><![endif]--> style='mso-ignore:vglayout;position:
 relative;z-index:251665408;left:-14px;top:0px;width:93px;height:37px'><img
 width=93 height=37 ![](/assets/summaries/{{page.id}}/image073.png" v:shapes="Picture_x0020_38"><![endif]><!--[if gte vml 1]></o:wrapblock><![endif]--><br
style='mso-ignore:vglayout' clear=ALL>
RxJS
 Pull data asyn through all layers. Only use when really needed.
Often no need to handle data as stream.

<!--[if gte vml 1]><v:shape id="Picture_x0020_41" o:spid="_x0000_s1035"
 type="#_x0000_t75" style='position:absolute;left:0;text-align:left;
 margin-left:88.95pt;margin-top:46.6pt;width:104.85pt;height:34.1pt;z-index:-251650048;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:page;
 mso-height-relative:page'>
 <v:imagedata ![](/assets/summaries/{{page.id}}/image074.png" o:title=""/>
</v:shape><![endif]--> style='mso-ignore:vglayout;position:
relative;z-index:-1895817216'> style='left:0px;position:absolute;
left:79px;top:-931px;width:105px;height:34px'><img width=105 height=34
![](/assets/summaries/{{page.id}}/image075.png" v:shapes="Picture_x0020_41"><![endif]><!--[if gte vml 1]><o:wrapblock><v:shape
  id="Picture_x0020_40" o:spid="_x0000_s1034" type="#_x0000_t75" style='position:absolute;
  left:0;text-align:left;margin-left:-4.85pt;margin-top:50.3pt;width:96.8pt;
  height:36.2pt;z-index:251667456;visibility:visible;mso-wrap-style:square;
  mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
  mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
  mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
  mso-position-horizontal-relative:text;mso-position-vertical:absolute;
  mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent:0;
  mso-width-relative:page;mso-height-relative:page'>
  <v:imagedata ![](/assets/summaries/{{page.id}}/image076.png" o:title=""/>
  <w:wrap type="topAndBottom"/>
 </v:shape><![endif]--> style='mso-ignore:vglayout;position:
 relative;z-index:251667456;left:-15px;top:0px;width:97px;height:37px'><img
 width=97 height=37 ![](/assets/summaries/{{page.id}}/image077.png" v:shapes="Picture_x0020_40"><![endif]><!--[if gte vml 1]></o:wrapblock><![endif]--><br
style='mso-ignore:vglayout' clear=ALL>
Data Access
 use @Angular/http module and register the service provider by
importing HttpModule to the Root Module.

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Routing</h2>



Routing is used for Navigation. After
bootstrap the initial routing will be done according to the current browser
URL. / The Router is an external (optional) NgModule RouterModule.<br>
It combines multiple directives: RouterOutlet, RouterLink, RouterLinkActive and
configuration=routes / history.pushState
is used for navigation but also # nav is possible.  class=Code>
 style='border:none'> style='border:none'>&lt;base
href=&quot;/&quot;&gt; is used by the
Browser to prefix relative URLs on refrences aswell the Angular static part
which will not be touched by the router. style='mso-spacerun:yes'>&nbsp;
 class=Code>  style='border:none'>
style='border:none'>providers: [{provide: APP_BASE_HREF, 
style='border:none'>useValue: '/'}]
 can be used to overwrite the global Variable.

Routes should be defined within their own
Routing Modules

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;mso-table-layout-alt:fixed;border:none;
 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
 0cm 0cm 0cm 0cm'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
  height:65.45pt'>
  <td width=93 valign=top style='width:92.55pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 0cm 0cm 0cm;height:65.45pt'>
  <div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
  mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242'>
  <p class=CodeBlockCxSpFirst>@NgModule({
  <b>declarations: [ <o:p></o:p>
  AppComponent
  ], <o:p></o:p>
  <b>imports
  style='mso-ansi-language:EN-GB'>: [ // ... <o:p></o:p>
  
  style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; AppRoutingModule<o:p></o:p>
  
  style='mso-spacerun:yes'>&nbsp; ],<o:p></o:p>
  <b>providers
  style='mso-ansi-language:EN-GB'>: [ <o:p></o:p>
  ], <o:p></o:p>
  <b>bootstrap
  style='mso-ansi-language:EN-GB'>: [AppComponent] }) <o:p></o:p>
  <b>export
  class AppModule { } <o:p></o:p>
  <o:p>&nbsp;</o:p>
  
  </td>
  <td width=93 valign=top style='width:92.6pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 0cm 0cm 0cm;height:65.45pt'>
  <div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
  mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
  background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
  242'>
  <b> style='color:#00006D;mso-ansi-language:
const  style='color:#377170;mso-ansi-language:EN-GB'>appRoutes
  style='mso-ansi-language:EN-GB'>: Routes = [<o:p></o:p>
  { <b>
  style='color:#B00004'>path: '', <b> style='color:#520067'>component:<o:p></o:p>
  
  style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  WelcomeComponent }  style='font-size:12.0pt;font-family:
  Times;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>
  ];<o:p></o:p>
  @NgModule({
  
_<o:p></o:p>
  <b> style='color:#520067;mso-ansi-language:
imports: [<o:p></o:p>
  
  style='mso-spacerun:yes'>&nbsp; RouterModule.<b><i>
  style='color:#B00004'>forRoot</i>(<b> style='color:#377170'>appRoutes)<o:p></o:p>
  
  style='mso-spacerun:yes'>&nbsp; ], <b> style='color:#520067'>exports:
  [ RouterModule ]  style='font-size:12.0pt;font-family:Times;
  mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>
  })
  
_<o:p></o:p>
  <p class=CodeBlockCxSpLast><b> style='color:#00006D;mso-ansi-language:
export class AppRoutingModule
  {}  style='font-size:12.0pt;font-family:Times;mso-bidi-font-family:
  Times;mso-ansi-language:EN-GB'><o:p></o:p>
  
  </td>
 </tr>
</table>

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst><b> style='color:#00006D;mso-ansi-language:
EN-GB'>const  style='color:#377170;mso-ansi-language:EN-GB'>appRoutes
style='mso-ansi-language:EN-GB'>: Routes = [ style='mso-fareast-font-family:
"MS Mincho";mso-bidi-font-family:"MS Mincho";mso-ansi-language:EN-GB'><o:p></o:p>

 style='mso-fareast-font-family:"MS Mincho";
mso-bidi-font-family:"MS Mincho";mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp; // crisis-center to CrisisListComponent<o:p></o:p>


style='mso-spacerun:yes'>&nbsp; { <b> style='color:#520067'>path:
<b> style='color:#0F7001'>'crisis-center', <b>
style='color:#520067'>component: CrisisListComponent }, <o:p></o:p>


style='mso-spacerun:yes'>&nbsp; // :id is variable value will be in id
parameter<o:p></o:p>


style='mso-spacerun:yes'>&nbsp; { <b> style='color:#520067'>path:
<b> style='color:#0F7001'>'hero/:id', <b>
style='color:#520067'>component: HeroDetailComponent },<o:p></o:p>


style='mso-spacerun:yes'>&nbsp; // redirect default route to /heroes
with exact match of path<o:p></o:p>

 style='color:black;mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp; { <b> style='color:#520067;
mso-ansi-language:EN-GB'>path style='color:black;mso-ansi-language:
EN-GB'>:  style='color:#0F7001;mso-ansi-language:EN-GB'>''
style='color:black;mso-ansi-language:EN-GB'>, <b> style='color:
#520067;mso-ansi-language:EN-GB'>redirectTo style='color:black;
mso-ansi-language:EN-GB'>:  style='color:#0F7001;mso-ansi-language:
EN-GB'>'/heroes' style='color:black;mso-ansi-language:EN-GB'>, <b>
style='color:#520067;mso-ansi-language:EN-GB'>pathMatch
style='color:black;mso-ansi-language:EN-GB'>:  style='color:#0F7001;
mso-ansi-language:EN-GB'>'full'  style='color:black;mso-ansi-language:
EN-GB'>},<o:p></o:p>

 style='color:black;mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp; // wildcard redirect (all other not yet
matched routes) style='font-family:"MS Mincho";mso-bidi-font-family:
"MS Mincho";color:black;mso-ansi-language:EN-GB'>_
style='font-size:12.0pt;mso-bidi-font-family:Times;color:black;mso-ansi-language:
EN-GB'><o:p></o:p>


style='mso-spacerun:yes'>&nbsp; { <b> style='color:#520067'>path:
<b> style='color:#0F7001'>'**', <b> style='color:#520067'>component:
PageNotFoundComponent } style='mso-fareast-font-family:"MS Mincho";
mso-bidi-font-family:"MS Mincho";mso-ansi-language:EN-GB'><o:p></o:p>

<p class=CodeBlockCxSpLast>]; 
style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>



ChildRouts
 a routed component can also declare ist own router outlet. Allowing
to extend the main routing rules and append ist own routs to the parent. It is
registered with the static forChild() method. The router displays the comp. Of
the child routes in router outlet of the child routing.

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst> style='color:#00006D;mso-ansi-language:EN-GB'>const
 style='color:#377170;mso-ansi-language:EN-GB'>routes
style='mso-ansi-language:EN-GB'>: Routes = [ style='font-family:
"MS Mincho";mso-bidi-font-family:"MS Mincho";mso-ansi-language:EN-GB'>_<o:p></o:p>

 style='font-family:"MS Mincho";mso-bidi-font-family:
"MS Mincho";mso-ansi-language:EN-GB'> style='mso-spacerun:yes'>&nbsp;
 style='color:#520067;mso-ansi-language:EN-GB'>path
style='mso-ansi-language:EN-GB'>:  style='color:#0F7001'>'samples',
 style='color:#520067'>component: SamplesComponent, 
style='font-size:12.0pt;font-family:Times;mso-bidi-font-family:Times;
mso-ansi-language:EN-GB'><o:p></o:p>

<b> style='color:#520067;mso-ansi-language:
EN-GB'> style='mso-spacerun:yes'>&nbsp; children
style='mso-ansi-language:EN-GB'>: [<o:p></o:p>


style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; {  style='color:#520067'>path:
 style='color:#0F7001'>':id',  style='color:#520067'>component:
SamplesDetailComponent }  style='font-size:12.0pt;font-family:Times;
mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>

]]; <o:p></o:p>

@NgModule({

EN-GB'>_<o:p></o:p>

 style='font-family:"MS Mincho";mso-bidi-font-family:
"MS Mincho";mso-ansi-language:EN-GB'> style='mso-spacerun:yes'>&nbsp;
<b> style='color:#520067;mso-ansi-language:EN-GB'>imports
style='mso-ansi-language:EN-GB'>: [ RouterModule.<b><i> style='color:#B00004'>forChild</i>(
style='color:#377170'>routes) ], <o:p></o:p>


style='mso-spacerun:yes'>&nbsp; <b> style='color:#520067'>exports:
[ RouterModule ]  style='font-size:12.0pt;font-family:Times;
mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>

})

EN-GB'>_<o:p></o:p>

<p class=CodeBlockCxSpLast><b> style='color:#00006D;mso-ansi-language:
EN-GB'>export class SamplesRoutingModule
{ } style='font-size:12.0pt;font-family:Times;mso-bidi-font-family:
Times;mso-ansi-language:EN-GB'><o:p></o:p>



Lazy
Routes they are loaded and invoked by the router
when needed. Loading and re-config is happening just once when router is first
requested. 

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>{ 
style='color:#520067'>path:  style='color:#0F7001'>'admin', 
style='color:#520067'>loadChildren:<o:p></o:p>

<p class=CodeBlockCxSpLast>
style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;  style='color:#0F7001'>'app/admin/
style='color:#B00004'>admin.module style='color:#0F7001'>#
style='color:#B00004'>AdminModule style='color:#0F7001'>', 
style='color:#520067'>canLoad: [AuthGuard] } <o:p></o:p>



canActivate
 will enforce the routert to load the Module even if Guard disallows
the action

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Forms</h2>



<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst> style='color:black;mso-ansi-language:EN-GB'>&lt;
style='color:#00006D;mso-ansi-language:EN-GB'>form 
style='color:blue;mso-ansi-language:EN-GB'>(ngSubmit)=
style='mso-ansi-language:EN-GB'>&quot;doLogin(<i>sampleForm</i>)&quot; 
style='color:#B00004'>#sampleForm=&quot;ngForm&quot;
style='color:black'>&gt; <o:p></o:p>

 style='color:black;mso-ansi-language:EN-GB'>&lt;
style='color:#00006D;mso-ansi-language:EN-GB'>input 
style='color:blue;mso-ansi-language:EN-GB'>type= style='mso-ansi-language:
EN-GB'>&quot;text&quot;  style='color:blue'>class=&quot;form-control&quot;
 style='color:blue'>id=&quot;name&quot;  style='color:blue'>required<o:p></o:p>

 style='color:blue;mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp; [(ngModel)]=
style='mso-ansi-language:EN-GB'>&quot; style='color:#676834'>model.name&quot;
 style='color:blue'>name=&quot;name&quot;  style='color:#B00004'>#name=&quot;ngModel&quot;
style='color:black'>&gt; style='font-family:"MS Mincho";
mso-bidi-font-family:"MS Mincho";color:black;mso-ansi-language:EN-GB'>_
style='mso-fareast-font-family:"MS Mincho";mso-bidi-font-family:"MS Mincho";
color:black;mso-ansi-language:EN-GB'><o:p></o:p>

 style='mso-fareast-font-family:"MS Mincho";
mso-bidi-font-family:"MS Mincho";color:black;mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp;  style='color:black;
mso-ansi-language:EN-GB'>&lt; style='color:#00006D;mso-ansi-language:
EN-GB'>div  style='color:blue;mso-ansi-language:EN-GB'>[hidden]=
style='mso-ansi-language:EN-GB'>&quot;<i> style='color:#B00004'>name</i>.
style='color:#520067'>valid || <i> style='color:#B00004'>name</i>.
style='color:#520067'>pristine&quot;  style='color:blue'>class=&quot;alert
alert-danger&quot; style='color:black'>&gt;  style='color:
#974613'>Name is required  style='color:black'>&lt;/
style='color:#00006D'>div style='color:black'>&gt;
style='font-size:12.0pt;mso-bidi-font-family:Times;color:black;mso-ansi-language:
EN-GB'><o:p></o:p>

 style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'> style='mso-spacerun:yes'>&nbsp;
&lt; style='color:#00006D;mso-ansi-language:EN-GB'>button 
style='color:blue;mso-ansi-language:EN-GB'>type= style='mso-ansi-language:
EN-GB'>&quot;submit&quot;  style='color:blue'>class=&quot;btn
btn-success&quot;<o:p></o:p>


style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 style='color:blue'>[disabled]= style='color:#0F7001'>&quot;!<i>sampleForm</i>.form.valid
style='color:#0F7001'>&quot; style='mso-bidi-font-family:
"Courier New";color:black;mso-ansi-language:EN-GB'>&gt;Submit&lt;/
style='color:#00006D;mso-ansi-language:EN-GB'>button
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>&gt;
style='mso-ansi-language:EN-GB'><o:p></o:p>

 style='color:black;mso-ansi-language:EN-GB'>&lt;
style='color:#00006D;mso-ansi-language:EN-GB'>form&gt;<o:p></o:p>

// ts
(Component)<o:p></o:p>

@Component({
... }) style='font-family:"MS Mincho";mso-bidi-font-family:"MS Mincho";
mso-ansi-language:EN-GB'>_ style='mso-fareast-font-family:"MS Mincho";
mso-bidi-font-family:"MS Mincho";mso-ansi-language:EN-GB'><o:p></o:p>

<b> style='color:#00006D;mso-ansi-language:
EN-GB'>export class SampleComponent
{  style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:
EN-GB'><o:p></o:p>

 style='color:#00006D;mso-ansi-language:EN-GB;
mso-bidi-font-weight:bold'> style='mso-spacerun:yes'>&nbsp; <b>public
<b> style='color:#520067;mso-ansi-language:EN-GB'>doLogin
style='mso-ansi-language:EN-GB'>(<b> style='color:#B00004'>f:NgForm):<b>
style='color:#00006D'>boolean { <o:p></o:p>


style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; <b> style='color:#00006D'>if
(<b> style='color:#B00004'>f.form.valid) { 
style='color:#959595'>// store data } style='font-family:
"MS Mincho";mso-bidi-font-family:"MS Mincho";mso-ansi-language:EN-GB'>_
style='mso-fareast-font-family:"MS Mincho";mso-bidi-font-family:"MS Mincho";
mso-ansi-language:EN-GB'><o:p></o:p>

 style='mso-fareast-font-family:"MS Mincho";
mso-bidi-font-family:"MS Mincho";mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; <b>
style='color:#00006D;mso-ansi-language:EN-GB'>return false
style='mso-ansi-language:EN-GB'>;  style='color:#959595'>// avoid postback
 style='font-size:12.0pt;mso-bidi-font-family:Times;
mso-ansi-language:EN-GB'><o:p></o:p>

<p class=CodeBlockCxSpLast>} }<o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###View Encapsulation<o:p></o:p></h2>



@Component({
encapsulation: ViewEncapsulation.&lt;Mode&gt; }) export class MyComp {}<o:p></o:p>


style='mso-ansi-language:EN-GB'>Mode style='mso-ansi-language:
EN-GB'>: Native (ShadowDOM) / Emulated (Preprocess and rename CSS code) / None
(Global)<o:p></o:p>

<div style='mso-element:para-border-div;border-top:none;border-left:solid #143553 3.0pt;
border-bottom:none;border-right:solid #143553 3.0pt;padding:0cm 2.0pt 0cm 2.0pt;
background:#1F4E79;mso-background-themecolor:accent1;mso-background-themeshade:
128'>

##React



React ist eine Library, kein Framework. To
build User Interfaces / The V in MVC
/ Minimal feature set. 

Goal
 split complex problem into multiple small ones to optimize:
wiederverwendbarkeit, Erweiterbarkeit, Wartbarkeit, Testbarkeit,
Aufgabenverteilung im Team. / Use functional programming. Components are
functions of type  class=Code>  style='border:none'>
style='border:none'>(At style='border:none'>tribute, State?) 
class=Code>  mso-ascii-font-family:
Consolas;Consolas;border:none;mso-char-type:symbol;
 style='border:none;mso-char-type:symbol;
� class=Code>
 style='border:none'> style='border:none'> View
 Komposition statt Vererbung, style='mso-spacerun:yes'>&nbsp;
Immutable.

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>function <i>
style='color:black'>App</i> style='color:black'>() { <o:p></o:p>


style='mso-spacerun:yes'>&nbsp; return  style='color:black'>( 
style='font-size:12.0pt;color:black;mso-ansi-language:EN-GB'><o:p></o:p>

 style='color:black;mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; &lt;
style='mso-ansi-language:EN-GB'>div style='color:black'>&gt;
style='font-family:"MS Mincho";mso-bidi-font-family:"MS Mincho";color:black;
mso-ansi-language:EN-GB'>_ style='color:black;mso-ansi-language:
EN-GB'>&lt;HelloMessage 
style='color:blue'>name style='color:#0F7001'>=&quot;HSR&quot;
style='color:black'>/&gt; &lt;img  style='color:blue'>src
style='color:#0F7001'>=&quot;/logo.png&quot; style='color:black'>/&gt;&lt;/div
style='color:black'>&gt; <o:p></o:p>

 style='color:black;mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp; ) style='font-size:12.0pt;
color:black;mso-ansi-language:EN-GB'><o:p></o:p>

<p class=CodeBlockCxSpLast> style='color:black;mso-ansi-language:EN-GB'>}<o:p></o:p>



JSX
 Pr�prozessor der JavaScript um XML erg�nzt. So kann quasi HTML
geschrieben werden mit eigenen Tags und allem, welches schlussendlich in reines
JS umgewandelt wird.  class=Code>  style='border:
none'> style='border:none'>&lt;List.Header&gt;
class=Code><u> 
style='border:none'> style='border:none'>{</u>
class=Code>  style='border:none'> style='border:none'>title.toUpper()
class=Code><u> 
style='border:none'> style='border:none'>}</u>
class=Code>  style='border:none'> style='border:none'>&lt;/List.Header&gt;
  class=Code>  style='border:none'>
style='border:none'>{} kann normale JS
expressions enthalten. Einschr�nkung
React Elemente m�ssen mit Uppercase beginnen und es k�nnen keine JS Keywords
verwendet werden.   
"Gill Sans";mso-char-type:
symbol; style='mso-char-type:symbol;
� um class
anzugeben muss className verwendet werden da class ein JS Keyword. <b
style='mso-bidi-font-weight:normal'>Immer:  class=Code>
 style='border:none'> style='border:none'>import React from
'react'
<o:p></o:p>

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>function Container(props) { 
 style='font-family:"MS Mincho";mso-bidi-font-family:"MS Mincho"'>_
return &lt;div className=&quot;container
style='color:#0F7001;mso-ansi-language:EN-GB'>&quot;
style='mso-ansi-language:EN-GB'>&gt;{<u style='text-underline:wave'>props.
style='color:#520067'>children</u>}&lt;/ style='color:#00006D'>div&gt;
<o:p></o:p>

} <o:p></o:p>

const App =
() =&gt; (<o:p></o:p>


style='mso-spacerun:yes'>&nbsp; &lt;Container&gt;<u style='text-underline:
wave'>&lt;HelloMessage name=&quot;HSR&quot; /&gt;</u>&lt;/Container&gt;<o:p></o:p>

<p class=CodeBlockCxSpLast>)<o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Styles &amp; Conditionals</h2>



Styles werden nicht als String sondern als
Objekt angegeben. Notation in camelCase!

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>const style = { display: 'flex',
width: '100%', minHeight: 300, }

<p class=CodeBlockCxSpLast>return &lt;div
style={style}&gt;{props.children}&lt;/div&gt;



Conditionals
 k�nnen innerhalb von  class=Code> 
style='border:none'> style='border:none'>{}
 verwendet werden. Was zu  class=Code> 
style='border:none'> style='border:none'>null, true, false, undefined
 evaluiert, wird nicht asugegeben. 

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlock>&lt;Container&gt;{error?&lt;span&gt;Fehler:
{error}&lt;/span&gt; : &lt;span&gt;OK!&lt;/span&gt;}



FunktionaleMethoden
 erlauben es das selbe auf mehrere Arten zu erreichen.

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst> class=s1><b>function 
class=s2><i>HelloMessage</i>(props) {<o:p></o:p>

 class=s1><b>
style='mso-spacerun:yes'>&nbsp; return  class=s2>
&lt; class=s1><b>div
class=s2>&gt;Hello {props. class=s3><b>
name class=s2>}&lt;/
class=s1><b>div class=s2>
&gt;<o:p></o:p>

<p class=CodeBlockCxSpMiddle align=center style='text-align:center;background:
#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:242'>
== ist gleich ==

<b> style='color:navy;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'>class  style='mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'>HelloMessage <b> style='color:navy'>extends
React.<i>Component </i>{ <o:p></o:p>

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'> style='mso-spacerun:yes'>&nbsp;  style='color:#7A7A43'>render()
{ <b> style='color:navy'>return &lt;<b> style='color:navy'>div&gt;Hello
{<b> style='color:navy'>this.props.<b> style='color:#660E7A'>name}&lt;/<b>
style='color:navy'>div&gt; }<o:p></o:p>

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'>}<o:p></o:p>

<p class=CodeBlockCxSpMiddle align=center style='text-align:center;background:
#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:242'>
style='mso-ansi-language:EN-GB;mso-fareast-language:EN-GB'>== ist gleich ==<o:p></o:p>

<p class=CodeBlockCxSpLast> class=s1><b>const 
class=s2><i>HelloMessage </i>=
({name}) =&gt; &lt; class=s1><b>div
class=s2>&gt;Hello {name}&lt;/ class=s1><b>
div class=s2>&gt;
style='font-size:18.0pt;mso-bidi-font-size:7.0pt;font-family:Times;mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB;mso-no-proof:no'><o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Props</h2>



Komponenten enthalten alle parameter als
props Objekt. Bei Klassen: this.props und bei Funktionen als Parameter. Props
sind immer Read-only / h�ufig mit 
class=Code>  style='border:none'> style='border:none'>{.
class=Code>  style='border:none'> style='border:none'>..props
class=Code>  style='border:none'> style='border:none'>}
 =Spread Operator verwendet um beliebige Anzahl an Props zu
zulassen.

Klassenvorteil
: Sie haben einen State sowie die M�glichkeit f�r Lifecycle-Hooks

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###State</h2>



Eine Komponente kann ein Objekt namens
state beinhalten. Diese Werte k�nnen wie props verwendet werden um angezigt zu
werden. Ausserdem kann ein solcher Wert �ber  class=Code>
 style='border:none'> style='border:none'>this.setState({
class=Code>  style='border:none'> style='border:none'>hits
class=Code>  style='border:none'> style='border:none'>: 
class=Code>  style='border:none'> style='border:none'>0
class=Code>  style='border:none'> style='border:none'>})
 setState nimmt das Objekt und merged dieses mit dem existierenden
state, nur die angegebenen Properties werden �berschrieben. <b
style='mso-bidi-font-weight:normal'>State abh�ngige changes sind
State�nderungen vom aktuellen State abh�ngig sollte man folgendes machen 
class=Code>  style='border:none'> style='border:none'>this.setState(
class=Code>  style='border:none'> style='border:none'>state
=&gt; ( class=Code>  style='border:
none'> style='border:none'>{ class=Code>
 style='border:none'> style='border:none'>hits
class=Code>  style='border:none'> style='border:none'>: 
class=Code>  style='border:none'> style='border:none'>state.counter
+ 1 class=Code>  style='border:none'>
style='border:none'>}) class=Code> 
style='border:none'> style='border:none'>)
 so kann man sicher gehen das der state zum Zeitpunkt abgetrennt vom
richtigen state ist. State updates k�nnen zusammengefasst
werden und laufen asynchron ab.
State ist immer privat, kann aber als prop an andere Komponente weitergegeben
werden   mso-ascii-font-family:
"Gill Sans";mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
 bei state �nderung wird auch die Komponente aktualisiert. Keine von
props abgeleitete Daten im state speichern, sondern direkt props daten
ableiten.

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Reconciliation</h2>



<!--[if gte vml 1]><v:shape id="Picture_x0020_45" o:spid="_x0000_s1033"
 type="#_x0000_t75" style='position:absolute;left:0;text-align:left;
 margin-left:120.45pt;margin-top:.75pt;width:78.2pt;height:23.75pt;z-index:251668480;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:page;
 mso-height-relative:page' wrapcoords="2286 0 2178 20008 21233 20008 21233 0 2286 0">
 <v:imagedata ![](/assets/summaries/{{page.id}}/image078.png" o:title=""/>
 <w:wrap type="tight"/>
</v:shape><![endif]--><img width=79 height=24
![](/assets/summaries/{{page.id}}/image079.png) v:shapes="Picture_x0020_45"><![endif]>
React berechnet alle �nderungen im DOM in einem Virtual DOM welches
mit dem richtigen DOM verglichen wird um sicher zu gehen, dass nur wirkliche
�nderungen am echten DOM durchgef�hrt werden.

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Input Handling &amp; Live verification</h2>



Take every input and imediately store it
immediately in the state and showd as value.

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>&lt;input
value={this.state.username}


style='mso-spacerun:yes'>&nbsp; onChange={this.handleUsernameChange} ...

handleUsernameChange = (event)
=&gt; { // validation here


style='mso-spacerun:yes'>&nbsp; this.setState({username: event.target.value})
}

<o:p>&nbsp;</o:p>

&lt;form
onSUbmit={this.handleSubmit}&gt;

<p class=CodeBlockCxSpLast>handleSubmit = (event) =&gt; {
event.preventDefault(); ... }



<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Styling &amp; Widget-Libraries</h2>



Diverse Layouts und Styles wurden in
Widget-Libraries zusammengefasst. (React-Semantic) darin werden React
Komponenten deklariert, welche als XML Tag wieder verwendet werden k�nnen. 
class=Code>  style='border:none'> style='border:none'>&lt;Form.Field
prop1=&quot;val1&quot;&gt; div um ein
input field inenrhalb Forms.

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Lifecycles</h2>



Mounting  


style='mso-char-type:symbol;�
   style='font-family:"Apple Color Emoji";
mso-bidi-font-family:"Apple Color Emoji"'>&#128257;
Updating   mso-ascii-font-family:
"Gill Sans";mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
 Unmounting

<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### Mounting</h3>



constructor(props)  


style='mso-char-type:symbol;�
 State initialisieren / render() / componentDidMount() 
 
mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
 DOM aufgebaut, time to load Remote data, start timers, setState
aufruf f�hrt zu re-rendering

<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### Updating</h3>



componentWillReceiveProps(nextProps) 
 
mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
 vorschau auf die n�chsten Props, davon abh�ngige Daten hier laden /
shouldComponentUpdate(nextProps, nextState)  


style='mso-char-type:symbol;�
 if return false then skip render / componentWillUpdate(nextProps,
nextState) / render() / componentDidUpdate(prevProps, prevState) 
 
mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
 analog zu componentDidMount

<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### Unmounting</h3>



componentWillUnmount() 
 
mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
 Aufr�umen, clearInterval(this.timerID)

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Guides &amp; Best practices</h2>



Ben�tig <b style='mso-bidi-font-weight:
normal'>State oder Lifecycle?
Dann Klassenkomponente sonst Funktionskomponente

Rollen
 Trennung zwischen Container-Komponenten und einfachen
Pr�sentations-Komponenten. Container kriegen eigene Klasse, Pr�sentations nur
eine Funktion.

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst><b> style='color:navy;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'>class  style='mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'>CommentListContainer <b>
style='color:navy'>extends React. style='color:#458383'>Component
{  style='color:#7A7A43'>constructor(props) {<o:p></o:p>

<b> style='color:navy;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'> style='mso-spacerun:yes'>&nbsp; super
style='mso-ansi-language:EN-GB;mso-fareast-language:EN-GB'>(props)<o:p></o:p>

<b> style='color:navy;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'> style='mso-spacerun:yes'>&nbsp; this
style='mso-ansi-language:EN-GB;mso-fareast-language:EN-GB'>.<b>
style='color:#660E7A'>state = { <b> style='color:#660E7A'>comments:
[] }<o:p></o:p>

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'>}<o:p></o:p>

 style='color:#7A7A43;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'>componentDidMount style='mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'>() { <o:p></o:p>

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'> style='mso-spacerun:yes'>&nbsp; fetch(<b>
style='color:green'>'/comments'). style='color:#7A7A43'>then(response
=&gt;&nbsp;<o:p></o:p>

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'> style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; response.json().
style='color:#7A7A43'>then(comments =&gt; <b> style='color:navy'>this.
style='color:#7A7A43'>setState({comments}) )<o:p></o:p>

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'> style='mso-spacerun:yes'>&nbsp; )<o:p></o:p>

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'>}<o:p></o:p>

<p class=CodeBlockCxSpLast><i> style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'>render </i> style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'>= () =&gt; &lt;<b> style='color:navy'>CommentList 
style='color:blue'>comments style='color:green'>={<b>
style='color:navy'>this.<b> style='color:#660E7A'>state.<b>
style='color:#660E7A'>comments}/&gt;<o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Redux</h2>



Frage
 wem geh�rt der Zustand der Applikation?

Redux ist einzig und allein verantwortlich
f�r den State. Es gibt einen Tree f�r den gesamten State der Applikation
(Single Source of Truth) / Tree ist immutable  


style='mso-char-type:symbol;�
 �nderungen am Tree resultieren in neuem Tree / State wird im Store
verwaltet / F�r �nderungen beschreibt man eine Action welche an den Store
gesendet wird.  class=Code>  style='border:none'>
style='border:none'>{type: 'ADD_TODO', text: 'Learn React'}
 / Reducer Mit einer
Reducer-Funktion wird der neue State t+1 erstellt.

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>Function todos(state = [],
action){


style='mso-spacerun:yes'>&nbsp; switch(action.type){


style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; case 'ADD_TODO': return [
...state, {text: action.text, completed: false}]


style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; default: return state

<p class=CodeBlockCxSpLast>
style='mso-spacerun:yes'>&nbsp; } style='mso-spacerun:yes'>&nbsp;
}



<!--[if gte vml 1]><v:shape id="Picture_x0020_46" o:spid="_x0000_s1032"
 type="#_x0000_t75" style='position:absolute;left:0;text-align:left;
 margin-left:130.45pt;margin-top:13.05pt;width:65.6pt;height:72.2pt;z-index:251669504;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:page;
 mso-height-relative:page' wrapcoords="1990 0 1616 20670 17410 20611 17659 -115 1990 0">
 <v:imagedata ![](/assets/summaries/{{page.id}}/image080.png" o:title=""/>
 <w:wrap type="tight"/>
</v:shape><![endif]--><img width=66 height=72
![](/assets/summaries/{{page.id}}/image081.png) v:shapes="Picture_x0020_46"><![endif]>
Ein Reducer ist immer f�r ein Teil (Slice) des State-Trees zust�ndig
(todos Array) Zus�tzlicher State hat eigenen Reducer (zB f�r Todo-Filter /
Kombination von mehreren Reducern zu einem root-reducer

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst><b> style='color:navy;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'>function <i> style='mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'>rootReducer</i> style='mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'>(state = {}, action) { <o:p></o:p>

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'> style='mso-spacerun:yes'>&nbsp; <b> style='color:navy'>return
{  style='font-size:9.0pt;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'><br>
<b> style='color:#660E7A;mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'> style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; todos
style='mso-ansi-language:EN-GB;mso-fareast-language:EN-GB'>: <i>todos</i>(state.<b>
style='color:#660E7A'>todos, action), <o:p></o:p>

<p class=CodeBlockCxSpLast><b> style='color:#660E7A;mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; filter
style='mso-ansi-language:EN-GB;mso-fareast-language:EN-GB'>: <i>filter</i>(state.<b>
style='color:#660E7A'>filter, action) }; }<o:p></o:p>



Jeder Reducer enth�lt nur teil des States
f�r den er zust�ndig ist, Resultat wird zu einem neuen State-Objekt kombiniert.
 

Abh�ngigkeiten
 Redux keine React abh�ngigkeit / React ContainerKomponenten m�ssen
zugriff auf den Redux State haben   style='font-family:
Wingdings;"Gill Sans";
mso-char-type:symbol; style='mso-char-type:
symbol;�
mapping von ReduxState nach ReactProps und Actions an den ReduxStore dispatchen
k�nnen.

<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### Verbinden von Redux mit React</h3>



<!--[if gte vml 1]><v:shapetype id="_x0000_t202"
 coordsize="21600,21600" o:spt="202" path="m0,0l0,21600,21600,21600,21600,0xe">
 <v:stroke joinstyle="miter"/>
 <v:path gradientshapeok="t" o:connecttype="rect"/>
</v:shapetype><v:shape id="Text_x0020_Box_x0020_56" o:spid="_x0000_s1031"
 type="#_x0000_t202" style='position:absolute;left:0;text-align:left;
 margin-left:208.7pt;margin-top:7.25pt;width:328.15pt;height:179.75pt;
 z-index:251677696;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
 mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent:0;
 mso-width-relative:margin;mso-height-relative:margin;v-text-anchor:top'
 o:gfxdata="UEsDBBQABgAIAAAAIQDkmcPA+wAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJQ5dIISSdEHaJSBUDjCyJ4nVZGx53NDeHictG4SKWNrj9//TuFwfx0FMGNg6quR9XkiB
pJ2x1FXyY7fNHqXgCGRgcISVPCHLdX17U+5OHlkkmriSfYz+SSnWPY7AufNIadK6MEJMx9ApD3oP
HapVUTwo7SgixSzOGbIuG2zhMESxOabrs0nCpXg+v5urKgneD1ZDTKJqnqpfuYADXwEnMj/ssotZ
nsglnHvr+e7S8JpWE6xB8QYhvsCYPJQJrHDlGqfz65Zz2ciZa1urMW8Cbxbqr2zjPing9N/wJmHv
OH2nq+WD6i8AAAD//wMAUEsDBBQABgAIAAAAIQAjsmrh1wAAAJQBAAALAAAAX3JlbHMvLnJlbHOk
kMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr28w6DZfS2o36h7xP//vCZFrUiS6Rs
YNf1oDA78jEHA++X49MLKKk2e7tQRgM3FDiMjw/7My62tiOZYxHVKFkMzLWWV63FzZisdFQwt81E
nGxtIwddrLvagHro+2fNvxkwbpjq5A3wye9AXW6lmf+wU3RMQlPtHCVN0xTdPaoObMsc3ZFtwjdy
jWY5YDXgWTQO1LKu/Qj6vn74p97TRz7jutV+h4zrj1dvuhy/AAAA//8DAFBLAwQUAAYACAAAACEA
czT1b3gCAABcBQAADgAAAGRycy9lMm9Eb2MueG1srFRRTxsxDH6ftP8Q5X1ce2qBVVxRB+o0CQEa
TDynuYSelsRZ4vau+/VzctfSsb0wTZWujv3ZsT/bubjsrGFbFWIDruLjkxFnykmoG/dc8W+Pyw/n
nEUUrhYGnKr4TkV+OX//7qL1M1XCGkytAqMgLs5aX/E1op8VRZRrZUU8Aa8cGTUEK5CO4bmog2gp
ujVFORqdFi2E2geQKkbSXvdGPs/xtVYS77SOCpmpOOWG+Rvyd5W+xfxCzJ6D8OtGDmmIf8jCisbR
pYdQ1wIF24Tmj1C2kQEiaDyRYAvQupEq10DVjEevqnlYC69yLURO9Aea4v8LK2+394E1dcWnp5w5
YalHj6pD9gk6Ririp/VxRrAHT0DsSE993usjKVPZnQ42/VNBjOzE9O7AboomSTkZn56V45IzSbay
PKffNMUpXtx9iPhZgWVJqHig9mVWxfYmYg/dQ9JtDpaNMbmFxv2moJi9RuUZGLxTJX3GWcKdUcnL
uK9KEwc58aTI06euTGBbQXMjpFQOc805LqETStPdb3Ec8Mm1z+otzgePfDM4PDjbxkHILL1Ku/6+
T1n3eKL6qO4kYrfqhg6voN5RgwP0KxK9XDbUhBsR8V4E2gnqKe053tFHG2grDoPE2RrCz7/pE55G
layctbRjFY8/NiIozswXR0P8cTyZpKXMh8n0rKRDOLasji1uY6+A2jGmF8XLLCY8mr2oA9gneg4W
6VYyCSfp7orjXrzCfvPpOZFqscggWkMv8MY9eJlCJ3rTiD12TyL4YQ6RRvgW9tsoZq/GsccmTweL
DYJu8qwmgntWB+JphfO0D89NeiOOzxn18ijOfwEAAP//AwBQSwMEFAAGAAgAAAAhAG65sTLeAAAA
CwEAAA8AAABkcnMvZG93bnJldi54bWxMj8FOwzAQRO9I/IO1SNyoXeoSCHEqBOIKaqGVuG3jbRIR
r6PYbcLf457guJqnmbfFanKdONEQWs8G5jMFgrjytuXawOfH6809iBCRLXaeycAPBViVlxcF5taP
vKbTJtYilXDI0UATY59LGaqGHIaZ74lTdvCDw5jOoZZ2wDGVu07eKnUnHbacFhrs6bmh6ntzdAa2
b4evnVbv9Ytb9qOflGT3II25vpqeHkFEmuIfDGf9pA5lctr7I9sgOgN6numEpkAvQZwBlS0yEHsD
i0wrkGUh//9Q/gIAAP//AwBQSwECLQAUAAYACAAAACEA5JnDwPsAAADhAQAAEwAAAAAAAAAAAAAA
AAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBLAQItABQABgAIAAAAIQAjsmrh1wAAAJQBAAALAAAA
AAAAAAAAAAAAACwBAABfcmVscy8ucmVsc1BLAQItABQABgAIAAAAIQBzNPVveAIAAFwFAAAOAAAA
AAAAAAAAAAAAACwCAABkcnMvZTJvRG9jLnhtbFBLAQItABQABgAIAAAAIQBuubEy3gAAAAsBAAAP
AAAAAAAAAAAAAAAAANAEAABkcnMvZG93bnJldi54bWxQSwUGAAAAAAQABADzAAAA2wUAAAAA
" filled="f" stroked="f"/><![endif]--> style='mso-ignore:vglayout;
position:relative;z-index:251677696'> style='left:0px;position:absolute;
left:199px;top:-1303px;width:330px;height:182px'>

<table cellpadding=0 cellspacing=0>
 <tr>
  <td><![endif]><![if !mso]> style='position:absolute;left:0pt;z-index:
  18'>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td><![endif]>
    <div v:shape="Text_x0020_Box_x0020_56" style='padding:3.6pt 7.2pt 3.6pt 7.2pt'
    class=shape>
    <o:p>&nbsp;</o:p>
    
    <![if !mso]></td>
   </tr>
  </table>
  <![endif]><![if !mso & !vml]>&nbsp;<![endif]></td>
 </tr>
</table>

<![endif]>React-Redux Library bietet daf�r
Funktion connect mit zwei parameter als funktion: 

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>connect(


style='mso-spacerun:yes'>&nbsp; mapStateToProps (state) =&gt; props,


style='mso-spacerun:yes'>&nbsp; mapDispatchToProps (dispatch) =&gt;
props

<p class=CodeBlockCxSpLast>)(TodoListComponent)



S2P erh�lt state und kann props ableiten /
D2P erh�lt einen Dispatcher und leitet props ab.

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>const mapStateToProps = (state)
=&gt; {


style='mso-spacerun:yes'>&nbsp; return { <u style='text-underline:wave'>todos</u>:
getVisible(state.todos, state.filter) }

<p class=CodeBlockCxSpLast>}



<u style='text-underline:wave'>todos</u>
 wird der Komponente als Props gesetzt

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst>const mapDispatchToProps =
(dispatch) =&gt; {


style='mso-spacerun:yes'>&nbsp; return { <u style='text-underline:wave'>onAddTodoClicked</u>:
(text) =&gt; { 

<p class=CodeBlockCxSpLast>
style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp; dispatch({type: 'ADD_TODO',
text: 'Text'}) style='mso-spacerun:yes'>&nbsp; }
style='mso-spacerun:yes'>&nbsp; } style='mso-spacerun:yes'>&nbsp;
}



function <u style='text-underline:wave'>onAddTodoClicked</u>
wird als props gesetzt

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst> class=Code>  style='color:
windowtext;border:none'> style='border:none'>const store =
createStore(rootReducer) class=Code> 
style='color:windowtext;border:none'><o:p></o:p>

<p class=CodeBlockCxSpLast> class=Code>  style='color:windowtext;
border:none'> style='border:none'>render(&lt;Provider store={store
style='border:none'>}&gt;&lt;App/&gt;&lt;/Provider&gt;,doc.gEbId('root'))
class=Code>  style='color:windowtext;border:none'><o:p></o:p>



Vorteile
 Bessere �bersicht da Zustand an einer Stelle / einfacheres
serialisieren / erm�glicht server rendering / einfacheres debugging da
zustands�nderungen explizit / time-traveling debugging / Chrome Redux Dev-Tools
/ React Komponenten werden einfacher. Verwenden?
Nur wenn zustand existiert der von mehreren Komponenten verwerdet werden muss /
Zudemm muss nicht alles in den ReduxStore (Formulardaten, MenuOpen) kann man
weiterhin im ReactState ablegen und halten.

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Testing</h2>



JEST (snapshot). <b style='mso-bidi-font-weight:
normal'>Init: const c = renderer.create(&lt;Comp /&gt;) / <b
style='mso-bidi-font-weight:normal'>Render let tree = c.toJSON()

Check
 tree.expect(tree).toMatchSnapshot(); / <b style='mso-bidi-font-weight:
normal'>Events tree.props.onMouseEnter(); class=Code>
style='font-family:"Gill Sans";mso-bidi-font-family:"Times New Roman";
mso-bidi-theme-font:minor-bidi;color:windowtext;border:none'><o:p></o:p>

<div style='mso-element:para-border-div;border-top:none;border-left:solid #143553 3.0pt;
border-bottom:none;border-right:solid #143553 3.0pt;padding:0cm 2.0pt 0cm 2.0pt;
background:#1F4E79;mso-background-themecolor:accent1;mso-background-themeshade:
128'>

##ASP.NET



<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Konzepte</h2>



ASP
 Server Side Scripting (wie PHP) / <b style='mso-bidi-font-weight:
normal'>ASP.NET Master Pages, Template Engine 


style='mso-char-type:symbol;�
HTML mit Code-Behind / ASP.NET
MVC ModelViewController, RazorEngine, OpenSource / <b style='mso-bidi-font-weight:
normal'>ASP.NET API RESTful Services, XML, JSON
style='mso-spacerun:yes'>&nbsp; / ASP.NET
Core MVC und WebAPI vereint (gleiche Interface / Klassen) Linux und Mac
f�hig, L�uft auf einem reduziertem .NET

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Middleware</h2>



Eingesetzt f�r Autorisierung, Logging, MVC,
WelcomePages, StaticFiles. Erster Parameter vom Konstruktor ist die n�chste
Middleware, weitere Parameter werden vom Dependency Container aufgel�st.

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'><!--[if gte vml 1]><v:shape id="Picture_x0020_47" o:spid="_x0000_i1027"
 type="#_x0000_t75" style='width:186pt;height:63pt;visibility:visible;
 mso-wrap-style:square'>
 <v:imagedata ![](/assets/summaries/{{page.id}}/image082.png" o:title=""/>
</v:shape><![endif]--><img width=186 height=63
![](/assets/summaries/{{page.id}}/image083.png" v:shapes="Picture_x0020_47"><![endif]>

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Razor Datentransfer</h2>



Ohne
Typensicherheit per Action  


style='mso-char-type:symbol;�
 ViewData or ViewBag (wrapper f�r ViewData) / per Request 
 
mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
 TempData (Daten �berleben einen Redirect) braucht Sessions

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Code</h2>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### Model</h3>



<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlock> style='color:#009695;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'>using style='mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'>&nbsp;System.ComponentModel.DataAnnotations;<br>
 style='color:#009695'>namespace&nbsp;Asp.Net.Uebung_01.Models<br>
{<br>
&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>public&nbsp;
style='color:#009695'>class&nbsp; style='color:#3363A4'>Bmi<br>
&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ style='color:#3363A4'>Display(Name&nbsp;=&nbsp;
style='color:#DB7100'>&quot;Gewicht&nbsp;in&nbsp;kg&quot;)]<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>public&nbsp;
style='color:#009695'>double&nbsp;Weight&nbsp;{&nbsp;
style='color:#009695'>get;&nbsp; style='color:#009695'>set;&nbsp;}<br>
&nbsp;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ style='color:#3363A4'>Display(Name&nbsp;=&nbsp;
style='color:#DB7100'>&quot;H�he&nbsp;in&nbsp;cm&quot;)]<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>public&nbsp;
style='color:#009695'>double&nbsp;Height&nbsp;{&nbsp;
style='color:#009695'>get;&nbsp; style='color:#009695'>set;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;}<br>
} style='font-family:"Times New Roman";mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'> <o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### View</h3>



_Layout.cshtml beinhaltet das Grundlayout.
&lt;head&gt; und &lt;body&gt; use  class=Code> 
style='border:none'> style='border:none'>@R style='border:none'>enderBody()
 to define where the Content is <u>rendered</u>. 
class=Code>  style='border:none'> style='border:none'>@RenderSection(&quot;scripts&quot;,
required: false) definiert eine Sektion
in welche die Content Page ihren Content <u>platzieren</u> kann. / 
class=Code>  style='border:none'> style='border:none'>@section
Scripts { @Scripts.Render(&quot;~/bund/jquery&quot;) }
class=Code>  style='border:none'><o:p></o:p>

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlock> style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'>&lt; style='color:#3363A4'>title&gt;@ViewData[&quot;Title&quot;]&nbsp;-&nbsp;Asp.Net.Uebung_01&lt;/
style='color:#3363A4'>title&gt; style='font-family:"Times New Roman";
mso-ansi-language:EN-GB;mso-fareast-language:EN-GB'> <o:p></o:p>



Bmi.cshtml

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlock> style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'>@model&nbsp;Asp.Net.Uebung_01.Models.Bmi<br>
@{<br>
&nbsp;&nbsp;&nbsp;&nbsp;ViewData[ style='color:#DB7100'>&quot;Title&quot;]&nbsp;=&nbsp;
style='color:#DB7100'>&quot;Home&nbsp;Page&quot;;<br>
&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>double&nbsp;
style='color:#009695'>value&nbsp;=&nbsp;ViewBag.Value;<br>
&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>var&nbsp;data&nbsp;=&nbsp;
style='color:#009695'>new[]<br>
&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>new&nbsp;{Text&nbsp;=&nbsp;
style='color:#DB7100'>&quot;starkes&nbsp;Untergewicht&quot;,&nbsp;Max=
style='color:#DB7100'>16.0},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;new&nbsp;{Text&nbsp;=&nbsp;&quot;m��iges&nbsp;Untergewicht&quot;,&nbsp;Max=17.0},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;new&nbsp;{Text&nbsp;=&nbsp;&quot;leichtes&nbsp;Untergewicht&quot;,&nbsp;Max=18.5},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;new&nbsp;{Text&nbsp;=&nbsp;&quot;Normalgewicht&quot;,&nbsp;Max=25.0},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;new&nbsp;{Text&nbsp;=&nbsp;&quot;Pr�adipositas&quot;,&nbsp;Max=30.0},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;new&nbsp;{Text&nbsp;=&nbsp;&quot;Adipositas&nbsp;Grad&nbsp;I&quot;,&nbsp;Max=35.0},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;new&nbsp;{Text&nbsp;=&nbsp;&quot;Adipositas&nbsp;Grad&nbsp;II&quot;,&nbsp;Max=40.0},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;new&nbsp;{Text&nbsp;=&nbsp;&quot;Adipositas&nbsp;Grad&nbsp;III&quot;,&nbsp;Max=999.0},<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;};<br>
}<br>
&lt; style='color:#3363A4'>p&gt;Ihr&nbsp;BMI:&nbsp;@value&lt;/
style='color:#3363A4'>p&gt;<br>
&lt; style='color:#3363A4'>table&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&lt; style='color:#3363A4'>thead&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&lt; style='color:#3363A4'>tr&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt; style='color:#3363A4'>th&gt;Beschreibung&lt;/
style='color:#3363A4'>th&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt; style='color:#3363A4'>th&gt;Von&lt;/
style='color:#3363A4'>th&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt; style='color:#3363A4'>th&gt;Bis&nbsp;&lt;/
style='color:#3363A4'>th&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/ style='color:#3363A4'>tr&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&lt;/ style='color:#3363A4'>thead&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;@{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>double&nbsp;old&nbsp;=&nbsp;
style='color:#DB7100'>0;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>foreach&nbsp;(
style='color:#009695'>var&nbsp;entry&nbsp; style='color:#009695'>in&nbsp;data)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;
style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&lt;tr&nbsp;style= style='color:#DB7100'>&quot;@(value&nbsp;&gt;&nbsp;old&nbsp;&amp;&amp;&nbsp;value&nbsp;&lt;=&nbsp;entry.Max&nbsp;?&nbsp;&quot;background:&nbsp;lightgray
style='color:#DB7100'>&quot;&nbsp;:&nbsp;&quot;&quot;)&quot;&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;@entry.Text&lt;/td&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;@old&lt;/td&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;td&gt;@entry.Max&lt;/td&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/tr&gt;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;old&nbsp;=&nbsp;entry.Max;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&lt;/ style='color:#3363A4'>table&gt;<br>
@section&nbsp;Scripts{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&lt; style='color:#3363A4'>script&nbsp;src=
style='color:#DB7100'>&quot;lib/jquery-ajax-unobtrusive/jquery.unobtrusive-ajax.min.js&quot;&gt;&lt;/
style='color:#3363A4'>script&gt;<br>
} style='font-family:"Times New Roman";mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'> <o:p></o:p>



Direkte nutzung von Services dank Injection
in Razor

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlock> style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'>@inject&nbsp;DataService&nbsp;DataService;&nbsp;@{<br>
&lt; style='color:#3363A4'>ul&gt;<br>
&nbsp;&nbsp;@foreach(&nbsp;var&nbsp;data&nbsp;in&nbsp;DataService.GetData())<br>
&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&lt; style='color:#3363A4'>li&gt;@data&lt;/
style='color:#3363A4'>li&gt;&nbsp;<br>
&nbsp;&nbsp;}<br>
&lt;/ style='color:#3363A4'>ul&gt;&nbsp;}
style='font-family:"Times New Roman";mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'> <o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### Controller</h3>



<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlock> style='color:#009695;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'>namespace style='mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'>&nbsp;Asp.Net.Uebung_01.Controllers<br>
{<br>
&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>public&nbsp;
style='color:#009695'>class&nbsp; style='color:#3363A4'>HomeController&nbsp;:&nbsp;
style='color:#3363A4'>Controller<br>
&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>private&nbsp;
style='color:#009695'>readonly&nbsp; style='color:#3363A4'>IBmiService&nbsp;_bmiService;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>public&nbsp;HomeController(
style='color:#3363A4'>IBmiService&nbsp;bmiService)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_bmiService&nbsp;=&nbsp;bmiService;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>public&nbsp;
style='color:#3363A4'>IActionResult&nbsp;Index()<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
style='color:#009695'>return&nbsp;View();<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>public&nbsp;
style='color:#3363A4'>IActionResult&nbsp;Bmi()<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
style='color:#009695'>return&nbsp;View();<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[ style='color:#3363A4'>HttpPost]<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>public&nbsp;
style='color:#3363A4'>IActionResult&nbsp;Bmi( style='color:#3363A4'>Bmi&nbsp;data)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
style='color:#009695'>if&nbsp;(data.Weight&nbsp;&gt;&nbsp;
style='color:#DB7100'>0&nbsp;&amp;&amp;&nbsp;data.Weight&nbsp;&lt;&nbsp;
style='color:#DB7100'>300&nbsp;<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&amp;&amp;&nbsp;data.Height&nbsp;&gt;&nbsp;
style='color:#DB7100'>30&nbsp;&amp;&amp;&nbsp;data.Height&nbsp;&lt;&nbsp;
style='color:#DB7100'>250)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ViewBag.Value&nbsp;=&nbsp;_bmiService.Calculcate(data);<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
style='color:#009695'>return&nbsp;PartialView( style='color:#DB7100'>&quot;Bmi&quot;);<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
style='color:#009695'>return&nbsp;Content( style='color:#DB7100'>&quot;Komische&nbsp;Daten&quot;);<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; style='color:#009695'>public&nbsp;
style='color:#3363A4'>IActionResult&nbsp;Bmi2( style='color:#3363A4'>Bmi&nbsp;data)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ViewBag.Value&nbsp;=&nbsp;_bmiService.Calculcate(data);<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
style='color:#009695'>return&nbsp;View( style='color:#DB7100'>&quot;Bmi&quot;);<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}<br>
&nbsp;&nbsp;&nbsp;&nbsp;}<br>
} style='mso-bidi-font-family:"Times New Roman";mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'> <o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

####  style='mso-ansi-language:EN-GB;mso-fareast-language:EN-GB'>Session
Handling<o:p></o:p></h3>



<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst> style='color:blue;mso-ansi-language:EN-GB'>public
 style='color:#257F9F;mso-ansi-language:EN-GB'>IActionResult 
style='mso-ansi-language:EN-GB'>Index() { style='font-family:"MS Mincho";
mso-bidi-font-family:"MS Mincho";mso-ansi-language:EN-GB'>_<o:p></o:p>

 style='font-family:"MS Mincho";mso-bidi-font-family:
"MS Mincho";mso-ansi-language:EN-GB'> style='mso-spacerun:yes'>&nbsp;
 style='color:blue;mso-ansi-language:EN-GB'>var 
style='mso-ansi-language:EN-GB'>value = (HttpContext.Session.GetInt32(SesssionKey)
?? 0) + 1;<o:p></o:p>


style='mso-spacerun:yes'>&nbsp; HttpContext.Session.SetInt32(SesssionKey,
value); style='font-family:"MS Mincho";mso-bidi-font-family:"MS Mincho";
mso-ansi-language:EN-GB'>_ style='color:blue;mso-ansi-language:
EN-GB'>return<o:p></o:p>

 style='color:blue;mso-ansi-language:EN-GB'>
style='mso-spacerun:yes'>&nbsp;  style='mso-ansi-language:
EN-GB'>Content( style='color:#900112'>$&quot;Dein {value}
style='color:#900112'>-xter Besuch&quot;); 
style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>

<p class=CodeBlockCxSpLast>} 
style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #5B9BD5 4.5pt;
mso-border-left-themecolor:accent1;border-bottom:none;border-right:solid #5B9BD5 4.5pt;
mso-border-right-themecolor:accent1;padding:0cm 0cm 0cm 0cm;background:#DEEAF6;
mso-background-themecolor:accent1;mso-background-themetint:51'>

#### Services</h3>



<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlock> style='color:#009695;mso-ansi-language:EN-GB;
mso-fareast-language:EN-GB'>namespace style='mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'>&nbsp;Asp.Net.Uebung_01.Services<br>
{<br>
&nbsp; style='color:#009695'>public&nbsp; style='color:#009695'>interface&nbsp;
style='color:#3363A4'>IBmiService{  style='color:#009695'>double&nbsp;Calculcate(
style='color:#3363A4'>Bmi&nbsp;data); }<br>
&nbsp; style='color:#009695'>public&nbsp; style='color:#009695'>class&nbsp;
style='color:#3363A4'>BmiService&nbsp;:&nbsp; style='color:#3363A4'>IBmiService
{<br>
&nbsp;&nbsp; style='color:#009695'>public&nbsp;
style='color:#009695'>double&nbsp;Calculcate( style='color:#3363A4'>Bmi&nbsp;data)
{<br>
&nbsp;&nbsp;&nbsp; style='color:#009695'>return&nbsp;
style='color:#3363A4'>Math.Round(data.Weight/ style='color:#3363A4'>Math.Pow((data.Height/
style='color:#DB7100'>100),&nbsp; style='color:#DB7100'>2),
style='color:#DB7100'>2);<br>
&nbsp;&nbsp;} } } style='font-family:"Times New Roman";mso-ansi-language:
EN-GB;mso-fareast-language:EN-GB'><o:p></o:p>



<div style='mso-element:para-border-div;border-top:none;border-left:solid #143553 3.0pt;
border-bottom:none;border-right:solid #143553 3.0pt;padding:0cm 2.0pt 0cm 2.0pt;
background:#1F4E79;mso-background-themecolor:accent1;mso-background-themeshade:
128'>

##Graphics



SVG
 Scalable VectorGraphics, sehr shnell flexibel, css &amp; JS, event
handling, sehr gute tools / f�r: Animationen / Grafiken / Charts / einfache
Spiele / nachteile: Performance

Canvas
 bitmap painting / f�r: viele Objekte / Nachteil: event-handling

WebGL
 3D, SHaders, GPU / f�r: echte Spiele / Nachteile: Sehr komplex

PureCSS
 als alternative zu Vektorgrafiken

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Polyline vs Polygon</h2>



<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst> style='font-family:"Courier New";color:black;
mso-ansi-language:EN-GB'>&lt; style='color:#00006D;mso-ansi-language:
EN-GB'>svg  style='color:blue;mso-ansi-language:EN-GB'>viewBox=
style='mso-ansi-language:EN-GB'>&quot;0 0 200 200&quot;
style='font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>&gt;
style='font-family:"MS Mincho";mso-bidi-font-family:"MS Mincho";color:black;
mso-ansi-language:EN-GB'>_ style='font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>&lt; style='color:#00006D;
mso-ansi-language:EN-GB'>polyline  style='color:blue;mso-ansi-language:
EN-GB'>points=&quot;0 0 200 0 200 100
100 200 0 200&quot;  style='color:blue'>stroke=&quot;red&quot; 
style='color:blue'>fill=&quot;blue&quot;  style='color:blue'>stroke-width=&quot;6&quot;
 style='font-family:"Courier New";color:black;mso-ansi-language:
EN-GB'>/&gt;  style='font-size:12.0pt;color:black;mso-ansi-language:
EN-GB'><o:p></o:p>

 style='font-family:"Courier New";color:black;
mso-ansi-language:EN-GB'>&lt;/ style='color:#00006D;mso-ansi-language:
EN-GB'>svg style='font-family:"Courier New";color:black;mso-ansi-language:
EN-GB'>&gt;  style='font-size:12.0pt;color:black;mso-ansi-language:
EN-GB'><o:p></o:p>

 style='font-family:"Courier New";color:black;
mso-ansi-language:EN-GB'>&lt; style='color:#00006D;mso-ansi-language:
EN-GB'>svg  style='color:blue;mso-ansi-language:EN-GB'>viewBox=
style='mso-ansi-language:EN-GB'>&quot;0 0 200 200&quot;
style='font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>&gt;
style='font-family:"MS Mincho";mso-bidi-font-family:"MS Mincho";color:black;
mso-ansi-language:EN-GB'>_ style='font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>&lt; style='color:#00006D;
mso-ansi-language:EN-GB'>polygon  style='color:blue;mso-ansi-language:
EN-GB'>points=&quot;0 0 200 0 200
100 100 200 0 200&quot;  style='color:blue'>stroke=&quot;blue&quot;
 style='color:blue'>fill=&quot;red&quot;  style='color:blue'>stroke-width=&quot;6&quot;
 style='font-family:"Courier New";color:black;mso-ansi-language:
EN-GB'>/&gt; <o:p></o:p>

<p class=CodeBlockCxSpLast> style='font-family:"Courier New";color:black;
mso-ansi-language:EN-GB'>/ style='color:#00006D;mso-ansi-language:
EN-GB'>svg style='font-family:"Courier New";color:black;mso-ansi-language:
EN-GB'>&gt;  style='font-size:12.0pt;color:black;mso-ansi-language:
EN-GB'><o:p></o:p>



Polyline
 ein Zug von X-Y Punkten welche mit einer Linie verbunden wird.
Start und Endpunkt k�nnen unterschiedlich sein / <b style='mso-bidi-font-weight:
normal'>Polygon stellt sicher dass die Geometrie geschlossen ist. (Blau hat
links keinen Border)

<o:p>&nbsp;</o:p>

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Path</h2>



<!--[if gte vml 1]><v:shapetype id="_x0000_t5" coordsize="21600,21600"
 o:spt="5" adj="10800" path="m@0,0l0,21600,21600,21600xe">
 <v:stroke joinstyle="miter"/>
 <v:formulas>
  <v:f eqn="val #0"/>
  <v:f eqn="prod #0 1 2"/>
  <v:f eqn="sum @1 10800 0"/>
 </v:formulas>
 <v:path gradientshapeok="t" o:connecttype="custom" o:connectlocs="@0,0;@1,10800;0,21600;10800,21600;21600,21600;@2,10800"
  textboxrect="0,10800,10800,18000;5400,10800,16200,18000;10800,10800,21600,18000;0,7200,7200,21600;7200,7200,14400,21600;14400,7200,21600,21600"/>
 <v:handles>
  <v:h position="#0,topLeft" xrange="0,21600"/>
 </v:handles>
</v:shapetype><v:shape id="Triangle_x0020_50" o:spid="_x0000_s1030" type="#_x0000_t5"
 style='position:absolute;left:0;text-align:left;margin-left:168.9pt;
 margin-top:10.4pt;width:19.7pt;height:20.65pt;rotation:180;z-index:251671552;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:margin;
 mso-height-relative:margin;v-text-anchor:middle' o:gfxdata="UEsDBBQABgAIAAAAIQDkmcPA+wAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJQ5dIISSdEHaJSBUDjCyJ4nVZGx53NDeHictG4SKWNrj9//TuFwfx0FMGNg6quR9XkiB
pJ2x1FXyY7fNHqXgCGRgcISVPCHLdX17U+5OHlkkmriSfYz+SSnWPY7AufNIadK6MEJMx9ApD3oP
HapVUTwo7SgixSzOGbIuG2zhMESxOabrs0nCpXg+v5urKgneD1ZDTKJqnqpfuYADXwEnMj/ssotZ
nsglnHvr+e7S8JpWE6xB8QYhvsCYPJQJrHDlGqfz65Zz2ciZa1urMW8Cbxbqr2zjPing9N/wJmHv
OH2nq+WD6i8AAAD//wMAUEsDBBQABgAIAAAAIQAjsmrh1wAAAJQBAAALAAAAX3JlbHMvLnJlbHOk
kMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr28w6DZfS2o36h7xP//vCZFrUiS6Rs
YNf1oDA78jEHA++X49MLKKk2e7tQRgM3FDiMjw/7My62tiOZYxHVKFkMzLWWV63FzZisdFQwt81E
nGxtIwddrLvagHro+2fNvxkwbpjq5A3wye9AXW6lmf+wU3RMQlPtHCVN0xTdPaoObMsc3ZFtwjdy
jWY5YDXgWTQO1LKu/Qj6vn74p97TRz7jutV+h4zrj1dvuhy/AAAA//8DAFBLAwQUAAYACAAAACEA
xzaVnKoCAADJBQAADgAAAGRycy9lMm9Eb2MueG1srFRLb9swDL4P2H8QdF8de0nXBHWKIEWGAUVb
rB16VmQpFqDXJCVO9utHSbb7PA3zwRBF8iP5ieTl1VFJdGDOC6NrXJ5NMGKamkboXY1/PW6+XGDk
A9ENkUazGp+Yx1fLz58uO7tglWmNbJhDAKL9orM1bkOwi6LwtGWK+DNjmQYlN06RAKLbFY0jHaAr
WVSTyXnRGddYZyjzHm6vsxIvEz7njIY7zj0LSNYYcgvp79J/G//F8pIsdo7YVtA+DfIPWSgiNAQd
oa5JIGjvxDsoJagz3vBwRo0qDOeCslQDVFNO3lTz0BLLUi1AjrcjTf7/wdLbw71DoqnxDOjRRMEb
PTpB9E4yBFfAT2f9Aswe7L3rJQ/HWOyRO4WcAVLLycUkfokDqAodE8WnkWJ2DIjCZTWdz7+eY0RB
VZ1XZXURQxQZK2Ja58N3ZhSKhxqHPpcETA43PmTzwSy6eCNFsxFSJsHttmvp0IHAi28265hUdnll
JjXqajyfVbOE/EqXmo+NIOFYvkeAjKWGxCM5mY50CifJYhZS/2QcaI0V5wCxoZ8xCaVMhzKrWtKw
nO8scdinO3gkehJgROZQ54jdAwyWGWTAzjC9fXRlaR5G5/xYb4p97Tx6pMhGh9FZCW3cR5VJqKqP
nO0HkjI1kaWtaU7QdKlxoOm8pRsBj31DfLgnDsYPLmGlhDv4cWngnUx/wqg17s9H99EepgK0GHUw
zjX2v/fEMYzkDw3zMi+n0zj/SZjOvlUguJea7UuN3qu1gf4pU3bpGO2DHI7cGfUEm2cVo4KKaAqx
a0yDG4R1yGsGdhdlq1Uyg5m3JNzoB0sjeGQ1NvLj8Yk4O3Q8jMqtGUafLN40fbaNntqs9sFwkSbi
mdeeb9gXqXH63RYX0ks5WT1v4OVfAAAA//8DAFBLAwQUAAYACAAAACEApTQMbt4AAAAJAQAADwAA
AGRycy9kb3ducmV2LnhtbEyPzU7DMBCE70i8g7VI3KhTByVVyKYCKq5IDZV6deMlSeufyHbbwNNj
TnBajXY08029no1mF/JhdBZhuciAke2cGm2PsPt4e1gBC1FaJbWzhPBFAdbN7U0tK+WudkuXNvYs
hdhQSYQhxqniPHQDGRkWbiKbfp/OGxmT9D1XXl5TuNFcZFnBjRxtahjkRK8Ddaf2bBAeX3Tp5+9d
q943tKL9drMvxBHx/m5+fgIWaY5/ZvjFT+jQJKaDO1sVmEbI8zKhRwSRpZsMeVkKYAeEQiyBNzX/
v6D5AQAA//8DAFBLAQItABQABgAIAAAAIQDkmcPA+wAAAOEBAAATAAAAAAAAAAAAAAAAAAAAAABb
Q29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgAAAAhACOyauHXAAAAlAEAAAsAAAAAAAAAAAAA
AAAALAEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgAAAAhAMc2lZyqAgAAyQUAAA4AAAAAAAAAAAAA
AAAALAIAAGRycy9lMm9Eb2MueG1sUEsBAi0AFAAGAAgAAAAhAKU0DG7eAAAACQEAAA8AAAAAAAAA
AAAAAAAAAgUAAGRycy9kb3ducmV2LnhtbFBLBQYAAAAABAAEAPMAAAANBgAAAAA=
" fillcolor="#ffc000" strokecolor="black [3213]"/><![endif]-->
style='mso-ignore:vglayout;position:relative;z-index:251671552'>
style='left:0px;position:absolute;left:155px;top:-1645px;width:28px;height:
29px'><img width=28 height=29 ![](/assets/summaries/{{page.id}}/image084.png" v:shapes="Triangle_x0020_50"><![endif]>
Sind das m�chtigste Werkzeug von Vektor-Grafiken / Erm�glichen es
komplexe Figuren zu definieren

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlock> style='color:#878787;mso-ansi-language:EN-GB'>&lt;
style='color:#850043;mso-ansi-language:EN-GB'>path 
style='color:#558A03;mso-ansi-language:EN-GB'>d style='color:#878787;
mso-ansi-language:EN-GB'>=&quot;M
100 100 L 300 100 L 200 300 z style='color:#878787'>&quot; 
style='color:#558A03'>fill style='color:#878787'>=&quot;orange
style='color:#878787'>&quot;  style='color:#558A03'>stroke
style='color:#878787'>=&quot;black style='color:#878787'>&quot; 
style='color:#558A03'>stroke-width style='color:#878787'>=&quot;3
style='color:#878787'>&quot; /&gt; style='font-size:12.0pt;
font-family:Times;mso-bidi-font-family:Times;color:black;mso-ansi-language:
EN-GB'><o:p></o:p>



 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'><!--[if gte vml 1]><v:shape id="Picture_x0020_49" o:spid="_x0000_i1026"
 type="#_x0000_t75" style='width:186pt;height:69pt;visibility:visible;
 mso-wrap-style:square'>
 <v:imagedata ![](/assets/summaries/{{page.id}}/image085.png" o:title=""/>
</v:shape><![endif]--><img width=186 height=69
![](/assets/summaries/{{page.id}}/image086.png" v:shapes="Picture_x0020_49"><![endif]>

 style='mso-ansi-language:EN-GB;mso-fareast-language:
EN-GB'><!--[if gte vml 1]><v:shape id="Picture_x0020_51" o:spid="_x0000_i1025"
 type="#_x0000_t75" style='width:185pt;height:62pt;visibility:visible;
 mso-wrap-style:square'>
 <v:imagedata ![](/assets/summaries/{{page.id}}/image087.png" o:title=""/>
</v:shape><![endif]--><img width=185 height=62
![](/assets/summaries/{{page.id}}/image088.png" v:shapes="Picture_x0020_51"><![endif]>
<br>
Arcs sind Teilabschnitte von einer
Ellipse, grosse Unterschiede zwischen Canvas und SVG.

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###SVG</h2>



<!--[if gte vml 1]><v:shape id="Picture_x0020_52" o:spid="_x0000_s1029"
 type="#_x0000_t75" style='position:absolute;left:0;text-align:left;
 margin-left:165.15pt;margin-top:25.4pt;width:34.75pt;height:18pt;z-index:251672576;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:page;
 mso-height-relative:page' wrapcoords="3689 950 3689 19200 19891 19200 19891 0 3689 950">
 <v:imagedata ![](/assets/summaries/{{page.id}}/image089.png" o:title=""/>
 <w:wrap type="tight"/>
</v:shape><![endif]--><img width=35 height=18
![](/assets/summaries/{{page.id}}/image090.png) v:shapes="Picture_x0020_52"><![endif]>
Gr�sse Browser kennt
gr�sse vom Inhalt nicht, default ist 300px/150px ist aber nicht standardisiert 
 
mso-symbol-font-family:
Wingdings'> style='mso-char-type:symbol;�
 Gr�sse immer angeben  class=Code> 
style='border:none'> style='border:none'>&lt;svg width=&quot;&quot;
height=&quot;&quot;&gt; das SVG wird aber
nicht skaliert, nur die Gr�sse der Fl�che wird definiert mit 
class=Code>  style='border:none'> style='border:none'>&lt;svg
 class=Code><u>  style='border:none'>
style='border:none'>viewBox</u> class=Code>
 style='border:none'> style='border:none'>=&quot;x y w
h&quot;&gt; bekommt man ein skalierbares
vektor image. / x: x offset y: y offset, width und height des zugrunde
liegenden koordinatensystems. preserveAspectRatio
nur wen viewBox definiert: verhalten wenn width und height nicht mit dem SVG
�bereinstimmen. <u>None</u> / <u>xMaxYMin</u> / <u>xMidYMid slice</u> / <u>xMidYMid
meet</u> style='mso-ansi-language:EN-GB;mso-fareast-language:EN-GB'><o:p></o:p>


style='mso-ansi-language:EN-GB;mso-fareast-language:EN-GB'>CSS
SVG und CSS sind
kompatibel auch Media-Queries / &lt;style&gt; und class=�� kann auf SVG
deklarationen angewendet werden um Farben und Gr�ssen zu �ndern. / <b
style='mso-bidi-font-weight:normal'>JS mit js kann mit svg objekten wie mit
normalen DOM objekten interagieren inkl. S�mtlichen Events. / <b
style='mso-bidi-font-weight:normal'>Libraries f�r komplexe Projekte mit
Library arbeiten.<o:p></o:p>

<div style='mso-element:para-border-div;border-top:none;border-left:solid #1F4E79 3.0pt;
mso-border-left-themecolor:accent1;mso-border-left-themeshade:128;border-bottom:
none;border-right:solid #1F4E79 3.0pt;mso-border-right-themecolor:accent1;
mso-border-right-themeshade:128;padding:0cm 2.0pt 0cm 2.0pt;background:#BDD6EE;
mso-background-themecolor:accent1;mso-background-themetint:102'>

###Canvas</h2>



<!--[if gte vml 1]><v:shape id="Picture_x0020_53" o:spid="_x0000_s1028"
 type="#_x0000_t75" style='position:absolute;left:0;text-align:left;
 margin-left:130.55pt;margin-top:.55pt;width:71.9pt;height:48.6pt;z-index:251673600;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:page;
 mso-height-relative:page' wrapcoords="2681 -209 2963 18167 20690 17957 20690 0 2681 -209">
 <v:imagedata ![](/assets/summaries/{{page.id}}/image091.png" o:title=""/>
 <w:wrap type="tight"/>
</v:shape><![endif]--><img width=71 height=48
![](/assets/summaries/{{page.id}}/image092.png) v:shapes="Picture_x0020_53"><![endif]>
Kennt nur Rectangle und Path / Wenn CSS und Canvasgr�sse
unterschiedlich, wird das Bild verzerrt. / State
der Kontext h�lt sich einen State (Farbe, Font, Stroke, Transforms,...) State
solange g�ltig bis �berschrieben. Save
speichert den kompletten state in einem stack <b style='mso-bidi-font-weight:
normal'>restore stellt den kompletten state vom stack wieder her (n�tzlich
bei Transformationen)

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst><b><i> style='mso-bidi-font-family:Times;
color:#520067;mso-ansi-language:EN-GB'>ctx</i>
style='mso-ansi-language:EN-GB'>. style='color:#676834'>save(); <i>
style='mso-bidi-font-family:Times;color:#6D6D6D;mso-ansi-language:EN-GB'>//state
speichern</i><i> style='font-family:"MS Mincho";mso-bidi-font-family:
"MS Mincho";color:#6D6D6D;mso-ansi-language:EN-GB'>_</i>
style='mso-ansi-language:EN-GB'>let <b><i> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>max </i>
style='mso-ansi-language:EN-GB'>=  style='color:blue'>250;

EN-GB'>_<b> style='mso-bidi-font-family:Times;color:#00006D;
mso-ansi-language:EN-GB'>for(let
<b><i> style='mso-bidi-font-family:Times;color:#520067;mso-ansi-language:
EN-GB'>x </i>= 
style='color:blue'>50; <b><i> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>x </i>
style='mso-ansi-language:EN-GB'>&lt;= <b><i> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>max</i>
style='mso-ansi-language:EN-GB'>; <b><i> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>x </i>
style='mso-ansi-language:EN-GB'>+=  style='color:blue'>50) { 
style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>

<b><i> style='mso-bidi-font-family:Times;
color:#520067;mso-ansi-language:EN-GB'>ctx</i>
style='mso-ansi-language:EN-GB'>.<b> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>strokeStyle 
style='mso-ansi-language:EN-GB'>= <b> style='mso-bidi-font-family:
Times;color:#0F7001;mso-ansi-language:EN-GB'>&quot;rgb(255,0,0)&quot;
style='mso-ansi-language:EN-GB'>; <i> style='mso-bidi-font-family:
Times;color:#6D6D6D;mso-ansi-language:EN-GB'>//state �ndern </i><b>
style='mso-bidi-font-family:Times;color:#00006D;mso-ansi-language:EN-GB'>if
style='mso-ansi-language:EN-GB'>(<b><i> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>x </i>
style='mso-ansi-language:EN-GB'>==  style='color:blue'>150)

EN-GB'>_{ 
style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>

<b><i> style='mso-bidi-font-family:Times;
color:#520067;mso-ansi-language:EN-GB'>ctx</i>
style='mso-ansi-language:EN-GB'>. style='color:#676834'>restore(); <i>
style='mso-bidi-font-family:Times;color:#6D6D6D;mso-ansi-language:EN-GB'>//state
wiederherstellen </i> style='font-size:12.0pt;mso-bidi-font-family:
Times;mso-ansi-language:EN-GB'><o:p></o:p>

<b><i> style='mso-bidi-font-family:Times;
color:#520067;mso-ansi-language:EN-GB'>ctx</i>
style='mso-ansi-language:EN-GB'>.<b> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>lineWidth 
style='mso-ansi-language:EN-GB'>=  style='color:blue'>10; <i>
style='mso-bidi-font-family:Times;color:#6D6D6D;mso-ansi-language:EN-GB'>//state
�ndern </i>} 
style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>

<b><i> style='mso-bidi-font-family:Times;
color:#520067;mso-ansi-language:EN-GB'>ctx</i>
style='mso-ansi-language:EN-GB'>. style='color:#676834'>beginPath();

EN-GB'>_<b><i> style='mso-bidi-font-family:Times;color:#520067;
mso-ansi-language:EN-GB'>ctx</i>.
style='color:#676834'>arc(<b><i> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>max</i>
style='mso-ansi-language:EN-GB'>+ style='color:blue'>5, <b><i>
style='mso-bidi-font-family:Times;color:#520067;mso-ansi-language:EN-GB'>max</i>
style='mso-ansi-language:EN-GB'>+ style='color:blue'>5, <b><i>
style='mso-bidi-font-family:Times;color:#520067;mso-ansi-language:EN-GB'>x</i>
style='mso-ansi-language:EN-GB'>,  style='color:blue'>0, <b>
style='mso-bidi-font-family:Times;color:#520067;mso-ansi-language:EN-GB'>Math
style='mso-ansi-language:EN-GB'>.<b> style='mso-bidi-font-family:
Times;color:#520067;mso-ansi-language:EN-GB'>PI 
style='mso-ansi-language:EN-GB'>*  style='color:blue'>2); <b><i>
style='mso-bidi-font-family:Times;color:#520067;mso-ansi-language:EN-GB'>ctx</i>
style='mso-ansi-language:EN-GB'>. style='color:#676834'>closePath();

EN-GB'>_<b><i> style='mso-bidi-font-family:Times;color:#520067;
mso-ansi-language:EN-GB'>ctx</i>.
style='color:#676834'>stroke();  style='font-size:12.0pt;
mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>

<p class=CodeBlockCxSpLast>} 
style='font-size:12.0pt;mso-bidi-font-family:Times;mso-ansi-language:EN-GB'><o:p></o:p>



<!--[if gte vml 1]><v:shape id="Picture_x0020_54" o:spid="_x0000_s1027"
 type="#_x0000_t75" style='position:absolute;left:0;text-align:left;
 margin-left:91.3pt;margin-top:56.5pt;width:150.75pt;height:63pt;rotation:-90;
 z-index:251674624;visibility:visible;mso-wrap-style:square;
 mso-width-percent:0;mso-height-percent:0;mso-wrap-distance-left:9pt;
 mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;
 mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;
 mso-position-horizontal-relative:text;mso-position-vertical:absolute;
 mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent:0;
 mso-width-relative:page;mso-height-relative:page' wrapcoords="15215 -8830 6510 -8830 6356 35756 15253 35756 15215 -8830">
 <v:imagedata ![](/assets/summaries/{{page.id}}/image093.png" o:title=""/>
 <w:wrap type="tight"/>
</v:shape><![endif]--><img width=73 height=160
![](/assets/summaries/{{page.id}}/image094.png) v:shapes="Picture_x0020_54"><![endif]>
Rotate and Translate != Translate and Rotate / <b style='mso-bidi-font-weight:
normal'>Double Buffering stellt kontinuierliche Bildfrequenz ohne Flackern
sicher (ist bei HTML Canvas �blicherweise nicht n�tig) / <b style='mso-bidi-font-weight:
normal'>Satische Hintergr�nde k�nnen mit zwei �berlagernden canvas
realisiert werden. Animationen
m�ssen manuell erstellt werden 1. Position bestimmen / 2. Canvas l�schen / 3.
Canvas neu zeichnen / 4. Timer neu stellen setInterval?
Achtet nicht auf die Zeit welche ben�tigt wird um das Bild zu zeichenen, daher
setTimeout verwenden. requestAnimationFrame
Browser ruft vor jedem repaint die renderfunction auf (60fps) stoppt oder
verlangsamt wenn im hintergrund / Animationen sollten nicht frame abh�ngig
sein. style='mso-spacerun:yes'>&nbsp; (use differenzzeit) 
class=Code>  style='border:none'> style='border:none'>window.requestAnimationFrame(paint)
 CSSTransition:
&lt;property&gt; &lt;duration&gt; &lt;timing-func&gt; &lt;delay&gt;; <b
style='mso-bidi-font-weight:normal'>Animation vs Transition: Trigger:
Transition werden ausgel�st bei prop �nderung, animationen direkt ausgel�st /
Looping nicht m�glich bei transition animationen erlauben repeat-count

<div style='mso-element:para-border-div;border:dashed windowtext 1.0pt;
mso-border-alt:dashed windowtext .5pt;padding:1.0pt 2.0pt 1.0pt 2.0pt;
background:#F2F2F2;mso-background-themecolor:background1;mso-background-themeshade:
242'>

<p class=CodeBlockCxSpFirst> style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>. style='mso-ansi-language:
EN-GB'>element  style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>{ style='font-family:"MS Mincho";
mso-bidi-font-family:"MS Mincho";color:black;mso-ansi-language:EN-GB'>_
style='color:blue;mso-ansi-language:EN-GB'>animation
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>:
fadein 
style='mso-bidi-font-family:"Courier New";color:blue;mso-ansi-language:EN-GB'>2
style='color:#0F7001;mso-ansi-language:EN-GB'>s  style='mso-bidi-font-family:
"Courier New";color:blue;mso-ansi-language:EN-GB'>1 
style='color:#0F7001;mso-ansi-language:EN-GB'>linear
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>;}
 style='font-size:12.0pt;color:black;mso-ansi-language:EN-GB'><o:p></o:p>

@keyframes
fadein  style='mso-bidi-font-family:"Courier New";color:black;
mso-ansi-language:EN-GB'>{ 
style='mso-spacerun:yes'>&nbsp;0%  style='mso-bidi-font-family:
"Courier New";color:black;mso-ansi-language:EN-GB'>{ 
style='color:blue;mso-ansi-language:EN-GB'>position
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>:
 style='color:#0F7001;mso-ansi-language:EN-GB'>relative
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>;
 style='color:blue;mso-ansi-language:EN-GB'>left
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>:
- style='mso-bidi-font-family:"Courier New";color:blue;mso-ansi-language:
EN-GB'>100 style='color:#0F7001;mso-ansi-language:EN-GB'>px
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>;
 style='color:blue;mso-ansi-language:EN-GB'>opacity
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>:
 style='mso-bidi-font-family:"Courier New";color:blue;mso-ansi-language:
EN-GB'>0.2 style='mso-bidi-font-family:"Courier New";color:black;
mso-ansi-language:EN-GB'>; } <o:p></o:p>


style='mso-spacerun:yes'>&nbsp;&nbsp; 99%  style='mso-bidi-font-family:
"Courier New";color:black;mso-ansi-language:EN-GB'>{ 
style='color:blue;mso-ansi-language:EN-GB'>opacity
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>:
 style='mso-bidi-font-family:"Courier New";color:blue;mso-ansi-language:
EN-GB'>0.2 style='mso-bidi-font-family:"Courier New";color:black;
mso-ansi-language:EN-GB'>;  style='color:blue;mso-ansi-language:
EN-GB'>display style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>:  style='color:#0F7001;
mso-ansi-language:EN-GB'>inline-block style='mso-bidi-font-family:
"Courier New";color:black;mso-ansi-language:EN-GB'>; }<o:p></o:p>


style='mso-spacerun:yes'>&nbsp; 100%  style='mso-bidi-font-family:
"Courier New";color:black;mso-ansi-language:EN-GB'>{ 
style='color:blue;mso-ansi-language:EN-GB'>opacity
style='mso-bidi-font-family:"Courier New";color:black;mso-ansi-language:EN-GB'>:
 style='mso-bidi-font-family:"Courier New";color:blue;mso-ansi-language:
EN-GB'>1 style='mso-bidi-font-family:"Courier New";color:black;
mso-ansi-language:EN-GB'>;  style='color:blue;mso-ansi-language:
EN-GB'>position style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>:  style='color:#0F7001;
mso-ansi-language:EN-GB'>relative style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>;  style='color:blue;
mso-ansi-language:EN-GB'>left style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>:  style='mso-bidi-font-family:
"Courier New";color:blue;mso-ansi-language:EN-GB'>0
style='color:#0F7001;mso-ansi-language:EN-GB'>px style='mso-bidi-font-family:
"Courier New";color:black;mso-ansi-language:EN-GB'>; } }<o:p></o:p>

 style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>let car =
document.createElement('canvas');<o:p></o:p>

 style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>let ctx = car.getContext('2d');<o:p></o:p>

 style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>ctx.fillStyle = 'rgb(255, 0, 255)';<o:p></o:p>

 style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>ctx.fillRect(0, 0, this.length,
this.height);<o:p></o:p>

<p class=CodeBlockCxSpLast><!--[if gte vml 1]><v:shape id="Text_x0020_Box_x0020_55"
 o:spid="_x0000_s1026" type="#_x0000_t202" style='position:absolute;
 margin-left:-3pt;margin-top:5.9pt;width:128.15pt;height:17.95pt;z-index:251675648;
 visibility:visible;mso-wrap-style:square;mso-width-percent:0;
 mso-height-percent:0;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;
 mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;
 mso-position-horizontal:absolute;mso-position-horizontal-relative:text;
 mso-position-vertical:absolute;mso-position-vertical-relative:text;
 mso-width-percent:0;mso-height-percent:0;mso-width-relative:margin;
 mso-height-relative:margin;v-text-anchor:top' o:gfxdata="UEsDBBQABgAIAAAAIQDkmcPA+wAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF
90jcwfIWJQ5dIISSdEHaJSBUDjCyJ4nVZGx53NDeHictG4SKWNrj9//TuFwfx0FMGNg6quR9XkiB
pJ2x1FXyY7fNHqXgCGRgcISVPCHLdX17U+5OHlkkmriSfYz+SSnWPY7AufNIadK6MEJMx9ApD3oP
HapVUTwo7SgixSzOGbIuG2zhMESxOabrs0nCpXg+v5urKgneD1ZDTKJqnqpfuYADXwEnMj/ssotZ
nsglnHvr+e7S8JpWE6xB8QYhvsCYPJQJrHDlGqfz65Zz2ciZa1urMW8Cbxbqr2zjPing9N/wJmHv
OH2nq+WD6i8AAAD//wMAUEsDBBQABgAIAAAAIQAjsmrh1wAAAJQBAAALAAAAX3JlbHMvLnJlbHOk
kMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr28w6DZfS2o36h7xP//vCZFrUiS6Rs
YNf1oDA78jEHA++X49MLKKk2e7tQRgM3FDiMjw/7My62tiOZYxHVKFkMzLWWV63FzZisdFQwt81E
nGxtIwddrLvagHro+2fNvxkwbpjq5A3wye9AXW6lmf+wU3RMQlPtHCVN0xTdPaoObMsc3ZFtwjdy
jWY5YDXgWTQO1LKu/Qj6vn74p97TRz7jutV+h4zrj1dvuhy/AAAA//8DAFBLAwQUAAYACAAAACEA
cwt5d3kCAABiBQAADgAAAGRycy9lMm9Eb2MueG1srFRNb9swDL0P2H8QdF+dGOlXUKfIWnQYULRF
26FnRZYSY5KoSUzs7NePkp0063bpsItNkY8U+Ujq4rKzhm1UiA24io+PRpwpJ6Fu3LLi355vPp1x
FlG4WhhwquJbFfnl7OOHi9ZPVQkrMLUKjIK4OG19xVeIfloUUa6UFfEIvHJk1BCsQDqGZVEH0VJ0
a4pyNDopWgi1DyBVjKS97o18luNrrSTeax0VMlNxyg3zN+TvIn2L2YWYLoPwq0YOaYh/yMKKxtGl
+1DXAgVbh+aPULaRASJoPJJgC9C6kSrXQNWMR2+qeVoJr3ItRE70e5ri/wsr7zYPgTV1xY+POXPC
Uo+eVYfsM3SMVMRP6+OUYE+egNiRnvq800dSprI7HWz6U0GM7MT0ds9uiiaT00l5Wo5LziTZyvKs
HJ2nMMWrtw8RvyiwLAkVD9S9TKrY3EbsoTtIuszBTWNM7qBxvykoZq9ReQQG71RIn3CWcGtU8jLu
UWmiIOedFHn41JUJbCNobISUymEuOccldEJpuvs9jgM+ufZZvcd575FvBod7Z9s4CJmlN2nX33cp
6x5PVB/UnUTsFl3u/b6fC6i31OYA/aJEL28a6sWtiPggAm0GdZa2He/pow20FYdB4mwF4eff9AlP
A0tWzlratIrHH2sRFGfmq6NRPh9PJmk182FyfFrSIRxaFocWt7ZXQF0Z07viZRYTHs1O1AHsCz0K
83QrmYSTdHfFcSdeYb//9KhINZ9nEC2jF3jrnrxMoRPLadKeuxcR/DCOSIN8B7udFNM3U9ljk6eD
+RpBN3lkE889qwP/tMh56IdHJ70Uh+eMen0aZ78AAAD//wMAUEsDBBQABgAIAAAAIQCGdrYc3QAA
AAgBAAAPAAAAZHJzL2Rvd25yZXYueG1sTI9NT8MwDIbvSPyHyEjctmRjH1CaTgjEFcRgk7h5jddW
NE7VZGv595gTHO3Xev08+Wb0rTpTH5vAFmZTA4q4DK7hysLH+/PkFlRMyA7bwGThmyJsisuLHDMX
Bn6j8zZVSko4ZmihTqnLtI5lTR7jNHTEkh1D7zHJ2Ffa9ThIuW/13JiV9tiwfKixo8eayq/tyVvY
vRw/9wvzWj35ZTeE0Wj2d9ra66vx4R5UojH9HcMvvqBDIUyHcGIXVWthshKVJPuZGEg+X5obUAcL
i/UadJHr/wLFDwAAAP//AwBQSwECLQAUAAYACAAAACEA5JnDwPsAAADhAQAAEwAAAAAAAAAAAAAA
AAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBLAQItABQABgAIAAAAIQAjsmrh1wAAAJQBAAALAAAA
AAAAAAAAAAAAACwBAABfcmVscy8ucmVsc1BLAQItABQABgAIAAAAIQBzC3l3eQIAAGIFAAAOAAAA
AAAAAAAAAAAAACwCAABkcnMvZTJvRG9jLnhtbFBLAQItABQABgAIAAAAIQCGdrYc3QAAAAgBAAAP
AAAAAAAAAAAAAAAAANEEAABkcnMvZG93bnJldi54bWxQSwUGAAAAAAQABADzAAAA2wUAAAAA
" filled="f" stroked="f"/><![endif]--> style='mso-ignore:vglayout;
position:relative;z-index:251675648'> style='position:absolute;left:-13px;
top:-1750px;width:130px;height:20px'>

<table cellpadding=0 cellspacing=0>
 <tr>
  <td><![endif]><![if !mso]> style='position:absolute;z-index:17'>
  <table cellpadding=0 cellspacing=0 width="100%">
   <tr>
    <td><![endif]>
    <div v:shape="Text_x0020_Box_x0020_55" style='padding:3.6pt 7.2pt 3.6pt 7.2pt'
    class=shape>
    
    style='mso-ansi-language:EN-GB'>Timing-func
    style='mso-ansi-language:EN-GB'>: <u>ease</u> / ease-in / ease-out /
    linear�<o:p></o:p>
    
    <![if !mso]></td>
   </tr>
  </table>
  <![endif]><![if !mso & !vml]>&nbsp;<![endif]></td>
 </tr>
</table>

<![endif]> style='mso-bidi-font-family:"Courier New";
color:black;mso-ansi-language:EN-GB'>return car;<o:p></o:p>